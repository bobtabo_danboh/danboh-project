#!/bin/sh
############################################################
# ENCODE    : UTF-8
# CREATE AT : 2009/11/19
#
# Copyright (c) Providence Co.,Ltd. All Rights Reserved.
############################################################

#===========================================================
# 定数設定
#===========================================================
JAVA_HOME="/usr/local/jdk"
LAUNCHE_LOG_DIR="/usr/local/ultramarine/sh/logs"

LAUNCHE_AT=`date +%Y%m%d`
LAUNCHE_PREFIX=jlauncher.
LAUNCHE_LOG=$LAUNCHE_LOG_DIR/$LAUNCHE_PREFIX$LAUNCHE_AT.log

#===========================================================
# バッチ設定
#===========================================================
BATCH_HOME=/usr/local/ultramarine/batch
JOB_NAME=rc-batch
OPTIONS=-jar
MAIN_CLASS=rc-batch-jar-with-dependencies.jar

#===========================================================
# 世代管理
#===========================================================
for tmp_f in `find $LAUNCHE_LOG_DIR -maxdepth 1 -type f -name "$LAUNCHE_PREFIX*" -mtime +6`
do
  rm $tmp_f
done

#===========================================================
# パラメータ設定
#===========================================================
COMMAND=$1

#===========================================================
# 状態チェック
#===========================================================
PS_LINE=`ps axwww | grep -e "rc-batch" | grep -v "^.*grep.*$"`
PS_CNT=`ps axwww | grep -e "rc-batch" | grep -c -v "^.*grep.*$"`
PID=`echo $PS_LINE | awk '{print $1}'`

#===========================================================
# コマンド実行処理
#===========================================================
#------------------------------
# start コマンド
#------------------------------
case "$1" in
    start)
    if [ $PS_CNT -eq 0 ]
    then
        Z_CNT=`echo $PS_LINE | awk '{print $3}' | grep -c -e "^.*Z.*$"`
        if [ $Z_CNT -ne 0 ]
        then
            echo "Killing $JOB_NAME Zombie Process."
            echo "[" `date` "] Killing $JOB_NAME Zombie Process." >> $LAUNCHE_LOG 
            kill -KILL $PID
        fi
        
        echo "Starting $JOB_NAME."
        echo "[" `date` "] Starting $JOB_NAME." >> $LAUNCHE_LOG 
        (/usr/bin/nohup $JAVA_HOME/bin/java $OPTIONS $BATCH_HOME/$MAIN_CLASS &) 1> /dev/null 2>> $LAUNCHE_LOG
    else
        echo "$JOB_NAME is already started."
        echo "[" `date` "] $JOB_NAME is already started." >> $LAUNCHE_LOG 
        exit 1
    fi
#------------------------------
# stop コマンド
#------------------------------
    ;;
    stop)
    if [ $PS_CNT -ne 0 ]
    then
        echo "Stopping $JOB_NAME."
        echo "[" `date` "] Stopping $JOB_NAME." >> $LAUNCHE_LOG
        kill -KILL $PID
    else
        echo "$JOB_NAME is already stopped."
        echo "[" `date` "] $JOB_NAME is already stopped." >> $LAUNCHE_LOG
        exit 1
    fi
#------------------------------
# status コマンド
#------------------------------
    ;;
    status)
        if [ $PS_CNT -ne 0 ]
        then
                echo "$JOB_NAME is running. [ $PS_CNT available process(es) ]"
        else
                echo "$JOB_NAME is stopped."
                exit 1
        fi
    ;;
    *)
        echo "Usage: $0 {start|stop|status}"
        exit 1
esac