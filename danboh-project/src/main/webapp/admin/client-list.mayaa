<?xml version="1.0" encoding="UTF-8"?>
<m:mayaa xmlns:m="http://mayaa.seasar.org" xmlns:s="/struts-tags"
    m:extends="./component/layout.html">
    <m:beforeRender>
        page.title="クライアント管理";
        page.subTitle="クライアント一覧";
        page.menu="client";
    </m:beforeRender>

    <!-- ページ機能 -->
    <m:insert id="counterArea" path="component/counter.html" />
    <m:insert id="pagerArea" path="component/pager.html" />

	<s:form m:id="form"  />

    <m:with id="keyword">
        <m:echo>
            <m:attribute name="name" value="keyword" />
            <m:attribute name="value" value="${keyword}" />
        </m:echo>
        <s:fielderror fieldName="keyword" />
    </m:with>

	<s:submit m:id="search" method="search" />

	<m:if id="isPresent-clientList" test="${isNotEmptyList(clientList)}">
		<m:echo>
			<m:doBody />
		</m:echo>
	</m:if>

	<m:if id="" test="${valid != null}"><m:echo><m:doBody /></m:echo></m:if>
	<m:if id="" test="${pause != null}"><m:echo><m:doBody /></m:echo></m:if>
	<m:if id="" test="${delete != null}"><m:echo><m:doBody /></m:echo></m:if>
    <m:with id="isPresent-valid">
        <m:if test="${st == null || st.value != Packages.org.danboh.web.enums.ClientStatus.VALID.value}">
            <m:doBody />
        </m:if>
        <m:if test="${st != null &amp;&amp; st.value == Packages.org.danboh.web.enums.ClientStatus.VALID.value}">
            <m:write value="${st.label}" />
        </m:if>
    </m:with>
    <m:with id="isPresent-pause">
        <m:if test="${st == null || st.value != Packages.org.danboh.web.enums.ClientStatus.PAUSE.value}">
            <m:doBody />
        </m:if>
        <m:if test="${st != null &amp;&amp; st.value == Packages.org.danboh.web.enums.ClientStatus.PAUSE.value}">
            <m:write value="${st.label}" />
        </m:if>
    </m:with>
    <m:with id="isPresent-delete">
        <m:if test="${st == null || st.value != Packages.org.danboh.web.enums.ClientStatus.DELETE.value}">
            <m:doBody />
        </m:if>
        <m:if test="${st != null &amp;&amp; st.value == Packages.org.danboh.web.enums.ClientStatus.DELETE.value}">
            <m:write value="${st.label}" />
        </m:if>
    </m:with>

    <m:with id="isPresent-standard">
        <m:if test="${pt == null || pt.value != Packages.org.danboh.web.enums.PactType.STANDARD.value}">
            <m:doBody />
        </m:if>
        <m:if test="${pt != null &amp;&amp; pt.value == Packages.org.danboh.web.enums.PactType.STANDARD.value}">
            <m:write value="${pt.label}" />
        </m:if>
    </m:with>
    <m:with id="isPresent-basicOem">
        <m:if test="${pt == null || pt.value != Packages.org.danboh.web.enums.PactType.BASIC_OEM.value}">
            <m:doBody />
        </m:if>
        <m:if test="${pt != null &amp;&amp; pt.value == Packages.org.danboh.web.enums.PactType.BASIC_OEM.value}">
            <m:write value="${pt.label}" />
        </m:if>
    </m:with>
    <m:with id="isPresent-fullOem">
        <m:if test="${pt == null || pt.value != Packages.org.danboh.web.enums.PactType.FULL_OEM.value}">
            <m:doBody />
        </m:if>
        <m:if test="${pt != null &amp;&amp; pt.value == Packages.org.danboh.web.enums.PactType.FULL_OEM.value}">
            <m:write value="${pt.label}" />
        </m:if>
    </m:with>

	<m:forEach id="forEach-clientList-client" items="${clientList}"
		var="client" index="index" replace="false"/>

	<m:write id="client.no" value="${offset + index + 1}" />
	<m:write id="client.status" value="${client.status.label}" />
	<m:write id="client.name" value="${client.name}" />
	<m:write id="client.postName" value="${client.postName}"
		default="&amp;nbsp;" />
	<m:write id="client.contactName" value="${client.contactName}" />
	<m:write id="client.pactType" value="${client.pactType.label}" />
	<m:write id="client.domain" value="${client.domain}" default="&amp;nbsp;" />
	<m:formatDate id="client.licenseLimitDate" value="${client.licenseLimitDate}"
		pattern="yyyy/MM/dd" default="&amp;nbsp;" />
	<m:with id="domainLink">
	   <m:if test="${isNotEmpty(client.domain)}">
		  <m:attribute name="href" value="${_.href}" />
		  <m:doBody />
	   </m:if>
	   <m:if test="${isEmpty(client.domain)}">
	       <m:write default="&amp;nbsp;" />
	   </m:if>
    </m:with>

	<m:echo id="infoLink">
		<m:attribute name="href" value="${_.href}?cid=${client.id}" />
		<m:doBody />
	</m:echo>

	<m:formatNumber id="userCount" value="${action.getUserCount(client.id)}" pattern="#,##0" default="0" />

    <!-- ソート -->
	<m:echo id="sortStatus">
        <m:attribute name="href" value="${_.href}?ob=${getOrderBy('status', ob)}" />
        <m:exec script="${var sort = getOrderImage('status', ob)}" />
        <m:if test="${isNotEmpty(sort)}">
            <m:element name="img">
                <m:attribute name="src" value="${sort}" />
            </m:element>
        </m:if>
		<m:doBody />
	</m:echo>
	<m:echo id="sortCompanyName">
        <m:attribute name="href" value="${_.href}?ob=${getOrderBy('companyName', ob)}" />
        <m:exec script="${var sort = getOrderImage('companyName', ob)}" />
        <m:if test="${isNotEmpty(sort)}">
            <m:element name="img">
                <m:attribute name="src" value="${sort}" />
            </m:element>
        </m:if>
		<m:doBody />
	</m:echo>
	<m:echo id="sortPostName">
        <m:attribute name="href" value="${_.href}?ob=${getOrderBy('postName', ob)}" />
        <m:exec script="${var sort = getOrderImage('postName', ob)}" />
        <m:if test="${isNotEmpty(sort)}">
            <m:element name="img">
                <m:attribute name="src" value="${sort}" />
            </m:element>
        </m:if>
		<m:doBody />
	</m:echo>
	<m:echo id="sortContactName">
        <m:attribute name="href" value="${_.href}?ob=${getOrderBy('contactName', ob)}" />
        <m:exec script="${var sort = getOrderImage('contactName', ob)}" />
        <m:if test="${isNotEmpty(sort)}">
            <m:element name="img">
                <m:attribute name="src" value="${sort}" />
            </m:element>
        </m:if>
		<m:doBody />
	</m:echo>
	<m:echo id="sortPactType">
        <m:attribute name="href" value="${_.href}?ob=${getOrderBy('pactType', ob)}" />
        <m:exec script="${var sort = getOrderImage('pactType', ob)}" />
        <m:if test="${isNotEmpty(sort)}">
            <m:element name="img">
                <m:attribute name="src" value="${sort}" />
            </m:element>
        </m:if>
		<m:doBody />
	</m:echo>
	<m:echo id="sortDomain">
        <m:attribute name="href" value="${_.href}?ob=${getOrderBy('domain', ob)}" />
        <m:exec script="${var sort = getOrderImage('domain', ob)}" />
        <m:if test="${isNotEmpty(sort)}">
            <m:element name="img">
                <m:attribute name="src" value="${sort}" />
            </m:element>
        </m:if>
		<m:doBody />
	</m:echo>
	<m:echo id="sortLicenseLimitDate">
        <m:attribute name="href" value="${_.href}?ob=${getOrderBy('licenseLimitDate', ob)}" />
        <m:exec script="${var sort = getOrderImage('licenseLimitDate', ob)}" />
        <m:if test="${isNotEmpty(sort)}">
            <m:element name="img">
                <m:attribute name="src" value="${sort}" />
            </m:element>
        </m:if>
		<m:doBody />
	</m:echo>
	<m:echo id="sortUserCount">
        <m:attribute name="href" value="${_.href}?ob=${getOrderBy('userCount', ob)}" />
        <m:exec script="${var sort = getOrderImage('userCount', ob)}" />
        <m:if test="${isNotEmpty(sort)}">
            <m:element name="img">
                <m:attribute name="src" value="${sort}" />
            </m:element>
        </m:if>
		<m:doBody />
	</m:echo>

</m:mayaa>
