function datepicker(id, options) {
  var default_options = {
    dateFormat: 'yy/mm/dd',
    changeYear: true,
    changeMonth:true,
    changeFirstDay:false,
    mandatory:false,
    dayNamesMin: ['日', '月', '火', '水', '木', '金', '土'],
    monthNamesShort: ['1月', '2月','3月', '4月', '5月','6月', '7月', '8月','9月', '10月', '11月','12月'],
    closeText: 'クリア'
  };

  if (options !== undefined) {
    for (var key in options) {
      default_options[key] = options[key];
    }
  }
  $('#' + id).datepicker(default_options);
}