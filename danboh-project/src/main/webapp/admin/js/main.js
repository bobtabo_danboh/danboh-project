function ContentCheckCallback(id, html, body) {
	var hasTextNode = function(element) {
		if (element.hasChildNodes()) {
			var child = element.childNodes;
			for (var i = 0; i < child.length; i++) {
				if (hasTextNode(child[i])) {
					return true;
				}
			}
			return false;
		} else {
			return element.nodeType == 3 && element.nodeValue != '';
		}
	};
	return (hasTextNode(body)) ? html : '';
}

function formSubmit(frm, method) {
	if (method && method != '') {
		$(frm).append("<input type=\"hidden\" name=\"method:"+ method + "\" />");
	}
	$(frm).submit();
}

// 削除前の確認メッセージ
function showConfirmBeforeDelete() {
	return window.confirm("削除しますか？");
}

// 確定前の確認メッセージ
function showConfirmBeforeFix() {
	return window.confirm("確定しますか？");
}

function cancelSubmit(e){   
	if (!e) {
		var e = window.event;
	}
	if(e.keyCode == 13) {
		return false;
	}
}

window.onload = function (){   
	var list = document.getElementsByTagName("input");
	for(var i=0; i<list.length; i++) {   
		if(list[i].type == 'text' || list[i].type == 'checkbox' || list[i].type == 'select' || list[i].type == 'file') {
			list[i].onkeypress = function (event){ return cancelSubmit(event); };
		}
	}
}

// フォームの二重送信防止
var disabledSubmit = false;
$(function() {
	// disabled 設定
	$('form').submit(function(){
		if (disabledSubmit) {
			return false;
		}
		disabledSubmit = true;
		setTimeout(function() {
			disabledSubmit = false;
		}, 10000);
	});
});
