package org.danboh.web.ext.struts2.validators;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;

import javax.imageio.ImageIO;
import javax.imageio.ImageReader;

import com.opensymphony.xwork2.validator.ValidationException;
import com.opensymphony.xwork2.validator.validators.FieldValidatorSupport;

/**
 * 指定された画像フォーマットの縦横サイズのチェックを行う。
 */
public class ImageSizeValidator extends FieldValidatorSupport {
	private String type;

	private int minHeight = -1;

	private int minWidth = -1;

	private int maxHeight = -1;

	private int maxWidth = -1;

	private String imageformats;

	public void validate(Object object) throws ValidationException {
		String fieldName = getFieldName();
		File val = (File) getFieldValue(fieldName, object);
		if (val == null) {
			return;
		}
		String[] formatList = imageformats.split(",");
		for (int i = 0; i < formatList.length; i++) {
			String imageformat = formatList[i];
			Iterator<ImageReader> ite = ImageIO.getImageReadersByFormatName(imageformat);
			while (true) {
				if (!ite.hasNext()) {
					break;
				}
				ImageReader reader = ite.next();
				if (reader == null) {
					reader = (ImageReader) ite.next();
				}
				try {
					reader.setInput(ImageIO.createImageInputStream(val));
					int height = reader.getHeight(0);
					int width = reader.getWidth(0);

					if ((getMinHeight() > -1) && (height < getMinHeight())) {
						addFieldError(fieldName, object);
					} else if ((getMaxHeight() > -1) && (height > getMaxHeight())) {
						addFieldError(fieldName, object);
					} else if ((getMinWidth() > -1) && (width < getMinWidth())) {
						addFieldError(fieldName, object);
					} else if ((getMaxWidth() > -1) && (width > getMaxWidth())) {
						addFieldError(fieldName, object);
					}
				} catch (IOException e) {
				} catch (IllegalArgumentException e) {
				} finally {
					if (reader != null) {
						reader.dispose();
					}
				}
			}
		}

	}

	public int getMaxHeight() {
		return maxHeight;
	}

	public void setMaxHeight(int maxHeight) {
		this.maxHeight = maxHeight;
	}

	public int getMaxWidth() {
		return maxWidth;
	}

	public void setMaxWidth(int maxWidth) {
		this.maxWidth = maxWidth;
	}

	public int getMinHeight() {
		return minHeight;
	}

	public void setMinHeight(int minHeight) {
		this.minHeight = minHeight;
	}

	public int getMinWidth() {
		return minWidth;
	}

	public void setMinWidth(int minWidth) {
		this.minWidth = minWidth;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getImageformats() {
		return imageformats;
	}

	public void setImageformats(String imageformats) {
		this.imageformats = imageformats;
	}

}
