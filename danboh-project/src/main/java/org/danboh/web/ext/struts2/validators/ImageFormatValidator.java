package org.danboh.web.ext.struts2.validators;


import java.io.File;
import java.io.IOException;
import java.util.Iterator;

import javax.imageio.IIOException;
import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.stream.ImageInputStream;

import org.danboh.web.exceptions.AppException;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.validator.ValidationException;
import com.opensymphony.xwork2.validator.validators.FieldValidatorSupport;

/**
 * 指定された画像フォーマットに合致しているかどうかのチェックを行う。
 */
public class ImageFormatValidator extends FieldValidatorSupport {
	private String imageformats;

	public void validate(Object object) throws ValidationException {
		String fieldName = getFieldName();
		File val = (File) getFieldValue(fieldName, object);
		if (val == null) {
			return;
		}
		String[] formatList = imageformats.split(",");
		for (int i = 0; i < formatList.length; i++) {
			String format = formatList[i];
			Iterator<ImageReader> ite = ImageIO.getImageReadersByFormatName(format);
			while (true) {
				if (!ite.hasNext()) {
					break;
				}
				ImageReader reader = (ImageReader) ite.next();
				ImageInputStream is = null;
				try {
					is = ImageIO.createImageInputStream(val);
					if (is != null) {
						reader.setInput(is);
						reader.getHeight(0);
					}
					return;
				} catch (IIOException e) {
					// this is format error
				} catch (IllegalArgumentException e) {
					// nothing to do
				} catch (IOException e) {
					throw new AppException(e);
				} finally {
					if (reader != null) {
						reader.dispose();
					}
					if (is != null) {
						try {
							is.close();
						} catch (Exception e) {
							throw new AppException(e);
						}
					}
				}
			}
		}

		// 画像関連のフィールドをクリア
		ActionContext.getContext().getValueStack().setValue(getFieldName(), null);
		ActionContext.getContext().getValueStack().setValue(getFieldName() + "ContentType", "");
		ActionContext.getContext().getValueStack().setValue(getFieldName() + "FileName", "");
		addFieldError(fieldName, object);
	}

	public String getImageformats() {
		return imageformats;
	}

	public void setImageformats(String imageformats) {
		this.imageformats = imageformats;
	}

}
