package org.danboh.web.ext.mayaa;


import org.seasar.mayaa.impl.engine.error.TemplateErrorHandler;

import org.danboh.web.util.ActionUtil;

/**
 * テンプレートエラーをハンドリングするクラスです。
 * 
 * @author <a href="mailto:kosugi@prov-co.com">kosugi</a>
 * @version $Revision: 1.1 $ $Date: 2010/06/11 04:03:41 $
 */
public class CustomTemplateErrorHandler extends TemplateErrorHandler {
	private static final long serialVersionUID = 3008302564830077616L;

	/**
	 * エラーページを返します。
	 * 
	 * @param throwableClass
	 *            例外クラス
	 * @return エラーページ
	 */
	@Override
	@SuppressWarnings("unchecked")
	protected String getPageName(Class throwableClass) {
		return "/" + ActionUtil.getNameSpace() + "/error/500";
	}
}
