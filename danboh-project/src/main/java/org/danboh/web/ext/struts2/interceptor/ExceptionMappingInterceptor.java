package org.danboh.web.ext.struts2.interceptor;

import java.util.List;

import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.TextProvider;
import com.opensymphony.xwork2.ValidationAware;
import com.opensymphony.xwork2.config.entities.ExceptionMappingConfig;
import com.opensymphony.xwork2.interceptor.ExceptionHolder;

/**
 * 発生した例外に応じてメッセージを設定します。
 */
public class ExceptionMappingInterceptor extends com.opensymphony.xwork2.interceptor.ExceptionMappingInterceptor {
	private static final long serialVersionUID = -3621616257045731423L;

	/**
	 * 例外を処理します。
	 * 
	 * @see com.opensymphony.xwork2.interceptor.ExceptionMappingInterceptor#publishException(com.opensymphony.xwork2.ActionInvocation,
	 *      com.opensymphony.xwork2.interceptor.ExceptionHolder)
	 */
	@Override
	protected void publishException(ActionInvocation invocation, ExceptionHolder exceptionHolder) {
		super.publishException(invocation, exceptionHolder);

		publishMessage(invocation, exceptionHolder);
	}

	/**
	 * 発生した例外に応じてメッセージを設定します。
	 * 
	 * @param invocation
	 *            ActionInvocation
	 * @param exceptionHolder
	 *            ExceptionHolder
	 */
	private void publishMessage(ActionInvocation invocation, ExceptionHolder exceptionHolder) {
		String message = null;
		String errorMessage = null;

		List<ExceptionMappingConfig> exceptionMappings = invocation.getProxy().getConfig().getExceptionMappings();
		if (exceptionMappings != null) {
			int deepest = Integer.MAX_VALUE;
			for (ExceptionMappingConfig exceptionMappingConfig : exceptionMappings) {
				int depth = getDepth(exceptionMappingConfig.getExceptionClassName(), exceptionHolder.getException());
				if (depth >= 0 && depth < deepest) {
					deepest = depth;

					message = (String) (exceptionMappingConfig.getParams().get("message"));
					TextProvider textProvider = (TextProvider) invocation.getAction();
					if (message != null) {
						message = textProvider.getText(message);
					}
					errorMessage = (String) (exceptionMappingConfig.getParams().get("errorMessage"));
					if (errorMessage != null) {
						errorMessage = textProvider.getText(errorMessage);
					}
				}
			}
		}

		Object action = invocation.getProxy().getAction();
		if (action instanceof ValidationAware) {
			if (message != null) {
				((ValidationAware) action).addActionMessage(message);
			}
			if (errorMessage != null) {
				((ValidationAware) action).addActionError(errorMessage);
			}
		}
	}
}
