package org.danboh.web.ext.struts2.validators;

import com.opensymphony.xwork2.validator.validators.RegexFieldValidator;

public class EmailValidator extends RegexFieldValidator {
	// see XW-371
	// public static final String emailAddressPattern =
	// "\\b(^['_A-Za-z0-9-]+(\\.['_A-Za-z0-9-]+)*@([A-Za-z0-9-])+(\\.[A-Za-z0-9-]+)*((\\.[A-Za-z0-9]{2,})|(\\.[A-Za-z0-9]{2,}\\.[A-Za-z0-9]{2,}))$)\\b";
	public static final String emailAddressPattern = "\\b(^['_.A-Za-z0-9-]+@([A-Za-z0-9-])+(\\.[A-Za-z0-9-]+)*((\\.[A-Za-z0-9]{2,})|(\\.[A-Za-z0-9]{2,}\\.[A-Za-z0-9]{2,}))$)\\b";

	public EmailValidator() {
		setExpression(emailAddressPattern);
		setCaseSensitive(false);
	}

}
