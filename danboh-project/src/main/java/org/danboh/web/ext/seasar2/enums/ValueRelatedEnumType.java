package org.danboh.web.ext.seasar2.enums;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.EnumSet;

import org.seasar.extension.jdbc.types.AbstractValueType;

public class ValueRelatedEnumType extends AbstractValueType {

	@SuppressWarnings("unchecked")
	private final Class<? extends Enum> enumClass;

	Method getMethod;

	@SuppressWarnings("unchecked")
	public ValueRelatedEnumType(Class<? extends Enum> enumClass) {
		super(Types.INTEGER);
		this.enumClass = enumClass;

		try {
			getMethod = enumClass.getMethod("getValue");
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		}
	}

	public Object getValue(ResultSet resultSet, int index) throws SQLException {
		return toEnum(resultSet.getInt(index));
	}

	@SuppressWarnings("unchecked")
	protected Enum toEnum(int value) {
		return valueOf(value);
	}

	@SuppressWarnings("unchecked")
	private Enum valueOf(int value) {
		try {
			for (Object enumField : EnumSet.allOf(enumClass)) {
				Integer enumValue = (Integer) getMethod.invoke(enumField, new Object[] {});
				if (value == enumValue) {
					return (Enum) enumField;
				}
			}
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}

		return null;
	}

	public Object getValue(ResultSet resultSet, String columnName) throws SQLException {

		return toEnum(resultSet.getInt(columnName));
	}

	public Object getValue(CallableStatement cs, int index) throws SQLException {
		return toEnum(cs.getInt(index));
	}

	public Object getValue(CallableStatement cs, String parameterName) throws SQLException {

		return toEnum(cs.getInt(parameterName));
	}

	public void bindValue(PreparedStatement ps, int index, Object value) throws SQLException {

		if (value == null) {
			setNull(ps, index);
		} else {
			ps.setInt(index, toInt(value));
		}
	}

	public void bindValue(CallableStatement cs, String parameterName, Object value) throws SQLException {

		if (value == null) {
			setNull(cs, parameterName);
		} else {
			cs.setInt(parameterName, toInt(value));
		}
	}

	public Integer toInt(Object value) {
		if (value == null) {
			return null;
		}
		try {
			return (Integer) getMethod.invoke(value, new Object[] {});
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}

		return null;
	}

	@Override
	public String toText(Object value) {
		return null;
	}

}
