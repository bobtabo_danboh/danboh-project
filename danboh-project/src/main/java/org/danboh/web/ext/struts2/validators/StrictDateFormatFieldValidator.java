package org.danboh.web.ext.struts2.validators;

import java.text.ParseException;

import org.apache.commons.lang.time.DateUtils;

import com.opensymphony.xwork2.validator.ValidationException;
import com.opensymphony.xwork2.validator.validators.FieldValidatorSupport;

/**
 * 日付形式の厳密なチェックを行います。
 */
public class StrictDateFormatFieldValidator extends FieldValidatorSupport {
	private String format;

	public void validate(Object object) throws ValidationException {
		Object obj = getFieldValue(getFieldName(), object);
		if (obj == null) {
			return;
		}
		if (obj.toString().length() == 0) {
			return;
		}
		try {
			DateUtils.parseDate(obj.toString(), new String[] { format });
		} catch (ParseException e) {
			addFieldError(getFieldName(), object);
		}
	}

	public String getFormat() {
		return format;
	}

	public void setFormat(String format) {
		this.format = format;
	}
}
