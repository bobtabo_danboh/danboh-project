package org.danboh.web.ext.struts2.convention;

import java.util.Map;

import org.apache.struts2.convention.ConventionsServiceImpl;

import com.opensymphony.xwork2.config.entities.PackageConfig;
import com.opensymphony.xwork2.config.entities.ResultTypeConfig;
import com.opensymphony.xwork2.inject.Inject;

public class MayaaSupportConventionsServiceImpl extends ConventionsServiceImpl {
	@Inject
	public MayaaSupportConventionsServiceImpl(@Inject("struts.convention.result.path") String resultPath) {
		super(resultPath);
	}

	@Override
	public Map<String, ResultTypeConfig> getResultTypesByExtension(PackageConfig packageConfig) {
		Map<String, ResultTypeConfig> results = packageConfig.getAllResultTypeConfigs();
		Map<String, ResultTypeConfig> resultTypesByExtension = super.getResultTypesByExtension(packageConfig);
		resultTypesByExtension.put("mayaa", results.get("mayaa"));

		return resultTypesByExtension;
	}
}
