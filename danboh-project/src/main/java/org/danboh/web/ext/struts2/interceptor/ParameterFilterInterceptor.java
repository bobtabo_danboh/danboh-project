package org.danboh.web.ext.struts2.interceptor;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;


import org.danboh.web.ext.struts2.interceptor.annotations.AllowByDefault;
import org.danboh.web.ext.struts2.interceptor.annotations.AllowFields;
import org.danboh.web.ext.struts2.interceptor.annotations.BlockByDefault;
import org.danboh.web.ext.struts2.interceptor.annotations.BlockFields;
import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.AbstractInterceptor;

public class ParameterFilterInterceptor extends AbstractInterceptor {
	private static final long serialVersionUID = -7199066470927349302L;
	public boolean defaultBlock = false;

	@Override
	public String intercept(ActionInvocation invocation) throws Exception {

		final Object action = invocation.getAction();
		Map<String, Object> parameters = invocation.getInvocationContext().getParameters();
		boolean isBlock = defaultBlock;
		// BlockByDefault の後勝ち
		if (action.getClass().isAnnotationPresent(AllowByDefault.class)) {
			isBlock = false;
		}
		if (action.getClass().isAnnotationPresent(BlockByDefault.class)) {
			isBlock = true;
		}
		List<String> annotatedFields = new ArrayList<String>();
		HashSet<String> paramsToRemove = new HashSet<String>();

		if (isBlock) {
			addAllAllowFields(action.getClass(), annotatedFields);

			for (String paramName : parameters.keySet()) {
				paramName = paramName.replaceAll("\\[\\d+\\]", "[*]");
				boolean allowed = false;
				for (String field : annotatedFields) {
					if (field.equals(paramName)) {
						allowed = true;
						break;
					}
				}

				if (!allowed) {
					paramsToRemove.add(paramName);
				}
			}
		} else {
			addAllBlockFields(action.getClass(), annotatedFields);

			for (String paramName : parameters.keySet()) {
				paramName = paramName.replaceAll("\\[\\d+\\]", "[*]");
				for (String field : annotatedFields) {
					if (field.equals(paramName)) {
						paramsToRemove.add(paramName);
						break;
					}
				}
			}
		}

		for (String aParamsToRemove : paramsToRemove) {
			parameters.remove(aParamsToRemove);
		}

		return invocation.invoke();
	}

	@SuppressWarnings("unchecked")
	public static void addAllAllowFields(Class clazz, List<String> allFields) {

		if (clazz == null) {
			return;
		}

		AllowFields ann = (AllowFields) clazz.getAnnotation(AllowFields.class);
		if (ann != null) {
			for (String field : ann.fields()) {
				allFields.add(field);
			}
		}
		addAllAllowFields(clazz.getSuperclass(), allFields);
	}

	@SuppressWarnings("unchecked")
	public static void addAllBlockFields(Class clazz, List<String> allFields) {

		if (clazz == null) {
			return;
		}

		BlockFields ann = (BlockFields) clazz.getAnnotation(BlockFields.class);
		if (ann != null) {
			for (String field : ann.fields()) {
				allFields.add(field);
			}
		}
		addAllAllowFields(clazz.getSuperclass(), allFields);
	}
}
