package org.danboh.web.ext.struts2.validators;

import java.text.ParseException;
import java.util.Date;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;

import com.opensymphony.xwork2.validator.ValidationException;
import com.opensymphony.xwork2.validator.validators.FieldValidatorSupport;

public class DateCompareFieldValidator extends FieldValidatorSupport {

	private String fromDate;
	private String toDate;

	public DateCompareFieldValidator() {
	}

	@Override
	public void validate(Object object) throws ValidationException {

		String fromDateVal = (String) getFieldValue(fromDate, object);
		String toDateVal = (String) getFieldValue(toDate, object);

		if (StringUtils.isEmpty(fromDateVal) || StringUtils.isEmpty(toDateVal)) {
			return;
		}

		Date from = convertDate(fromDateVal);
		Date to = convertDate(toDateVal);

		if (from == null || to == null) {
			return;
		}

		if (from.compareTo(to) > 0) {
			addFieldError(getFieldName(), object);
		}

	}

	private Date convertDate(String yyyymmdd) {
		try {
			return DateUtils.parseDate(yyyymmdd, new String[] { "yyyyMMdd", "yyyy/MM/dd" });
		} catch (ParseException e) {
			return null;
		}
	}

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getToDate() {
		return toDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}
}
