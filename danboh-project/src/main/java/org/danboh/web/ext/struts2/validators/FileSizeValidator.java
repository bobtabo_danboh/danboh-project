package org.danboh.web.ext.struts2.validators;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.validator.ValidationException;
import com.opensymphony.xwork2.validator.validators.FieldValidatorSupport;

/**
 * 指定されたファイルのサイズチェックを行う。
 */
public class FileSizeValidator extends FieldValidatorSupport {
	private int max = -1;

	public void validate(Object object) throws ValidationException {
		String fieldName = getFieldName();
		File val = (File) getFieldValue(fieldName, object);
		if (val == null) {
			return;
		}
		try {
			if (val.isFile() && FileUtils.readFileToByteArray(val).length > max) {
				addFieldError(fieldName, object);
				ActionContext.getContext().getValueStack().setValue(getFieldName(), null);
			}
		} catch (IOException e) {
		}
	}

	public String getDisplayMax() {
		return FileUtils.byteCountToDisplaySize(max);
	}

	public int getMax() {
		return max;
	}

	public void setMax(int max) {
		this.max = max;
	}

}
