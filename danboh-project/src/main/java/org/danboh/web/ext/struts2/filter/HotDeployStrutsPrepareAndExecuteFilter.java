package org.danboh.web.ext.struts2.filter;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import org.apache.struts2.dispatcher.ng.filter.StrutsPrepareAndExecuteFilter;
import org.seasar.framework.container.S2Container;
import org.seasar.framework.container.factory.SingletonS2ContainerFactory;
import org.seasar.framework.container.util.SmartDeployUtil;

/**
 * HotDeploy対応のStrutsPrepareAndExecuteFilter。
 * HotDeployモードの場合はリクエストの度に設定を再読み込みします。
 */
public class HotDeployStrutsPrepareAndExecuteFilter extends StrutsPrepareAndExecuteFilter {
	private FilterConfig filterConfig;
	private S2Container container;

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		this.filterConfig = filterConfig;
		container = SingletonS2ContainerFactory.getContainer();
		if (container == null || !SmartDeployUtil.isHotdeployMode(container)) {
			super.init(filterConfig);
		}
	}

	public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException,
			ServletException {
		if (container != null && SmartDeployUtil.isHotdeployMode(container)) {
			super.init(filterConfig);
		}
		super.doFilter(req, res, chain);
		if (container != null && SmartDeployUtil.isHotdeployMode(container)) {
			super.destroy();
		}
	}

	public void destroy() {
		if (container == null || !SmartDeployUtil.isHotdeployMode(container)) {
			super.destroy();
		}
	}
}
