package org.danboh.web.ext.struts2.convention;

import javax.servlet.ServletContext;

import org.apache.struts2.convention.ConventionsService;
import org.apache.struts2.convention.DefaultResultMapBuilder;

import com.opensymphony.xwork2.inject.Inject;

public class MayaaSupportResultMapBuilder extends DefaultResultMapBuilder {

	@Inject
	public MayaaSupportResultMapBuilder(ServletContext servletContext,
			@Inject("struts.convention.resultMapBuilde") ConventionsService conventionsService,
			@Inject("struts.convention.relative.result.types") String relativeResultTypes) {
		super(servletContext, conventionsService, relativeResultTypes);
	}
}
