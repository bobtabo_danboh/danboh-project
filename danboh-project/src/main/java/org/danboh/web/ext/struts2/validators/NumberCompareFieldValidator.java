package org.danboh.web.ext.struts2.validators;

import org.apache.commons.lang.StringUtils;

import com.opensymphony.xwork2.validator.ValidationException;
import com.opensymphony.xwork2.validator.validators.FieldValidatorSupport;

/**
 * 数値のFrom～To比較を行うバリデーションクラスです。
 * 
 * @author <a href="mailto:nagashiba@prov-co.com">Satoshi Nagashiba</a>
 * @version $Revision: 1.1 $ $Date: 2010/06/11 04:03:39 $
 */
public class NumberCompareFieldValidator extends FieldValidatorSupport {

	private String fromInt;
	private String toInt;

	public NumberCompareFieldValidator() {
	}

	@Override
	public void validate(Object object) throws ValidationException {

		String fromIntVal = (String) getFieldValue(fromInt, object);
		String toInteVal = (String) getFieldValue(toInt, object);

		if (StringUtils.isEmpty(fromIntVal) || StringUtils.isEmpty(toInteVal)) {
			return;
		}

		Integer from;
		Integer to;
		try {
			from = convertInt(fromIntVal);
			to = convertInt(toInteVal);
		} catch (NumberFormatException e) {
			return;
		}

		if (from == null || to == null) {
			return;
		}

		if (from.compareTo(to) > 0) {
			addFieldError(getFieldName(), object);
		}
	}

	private Integer convertInt(String number) throws NumberFormatException {
		return Integer.parseInt(number);
	}

	public String getFromInt() {
		return fromInt;
	}

	public void setFromInt(String fromInt) {
		this.fromInt = fromInt;
	}

	public String getToInt() {
		return toInt;
	}

	public void setToInt(String toInt) {
		this.toInt = toInt;
	}
}
