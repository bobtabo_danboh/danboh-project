/*
 * Danboh is a Source Code Generation program developed by BobTabo.
 * 
 * Copyright (c) 2009 BobTabo. All Rights Reserved.
 */
package org.danboh.web.listener;

/* $Id: StartupServletContextListener.java,v 1.3 2010/06/30 20:35:53 nagashiba Exp $ */

import java.io.File;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import net.sf.jasperreports.engine.JasperCompileManager;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * WebアプリケーションにあるServletコンテキストの変更に関する通知を受け取るクラスです。
 * 
 * @author Minagawa
 * @version $Revision: 1.3 $ $Date: 2010/06/30 20:35:53 $
 */
public class StartupServletContextListener implements ServletContextListener {

	/** このクラスで発生した実行状況をログに記録するためのオブジェクトです。 */
	private static final Log logger = LogFactory.getLog(StartupServletContextListener.class);

	/**
	 * Webアプリケーションがリクエストを処理する準備ができたことを通知します。
	 * 
	 * @param event
	 *            Servletコンテキストに変更があったことを通知するためのイベント
	 */
	public void contextInitialized(ServletContextEvent event) {
		try {
			logger.debug("initialize start");

			// Jasperを初回起動時にサーバ上でコンパイルする。
			// サブレポート
			File file = new File(getClass().getResource("/report/search-rank_subreport.jrxml").getFile());
			JasperCompileManager
					.compileReportToFile(file.getPath(), file.getParent() + "/search-rank_subreport.jasper");
			// メインレポート
			file = new File(getClass().getResource("/report/search-rank.jrxml").getFile());
			JasperCompileManager.compileReportToFile(file.getPath(), file.getParent() + "/search-rank.jasper");

			logger.debug("initialize end");
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new RuntimeException(e);
		}
	}

	/**
	 * Servletコンテキストがシャットダウン処理に入ることを通知します。
	 * 
	 * @param event
	 *            Servletコンテキストに変更があったことを通知するためのイベント
	 */
	public void contextDestroyed(ServletContextEvent event) {
		try {
			logger.debug("destroy start");

			logger.debug("destroy end");

		} catch (Exception e) {
			logger.error(e.getMessage());
		}
	}
}