package org.danboh.web.report.data;

/**
 * 検索順位レポートのデータソース抽象クラスです。
 */
@javax.annotation.Generated("danboh")
public abstract class AbstractSearchRankDataSource extends org.danboh.web.report.ReportBeanDataSource {

	/** 検索順位レポートのプロパティ判別オブジェクトであることを意味します */
	private static final ReportPropertyNameProvider SearchRankProvider = new BaseReportPropertyNameProvider() {
		protected String[] getPropertyNames() {
			return new String[] {
                    "siteName"
                    ,"siteUrl"
                    ,"searchPhrase"
                    ,"graphLabel"
                    ,"imagePath"
                    ,"memoData"
                    ,"yahooRankIn"
                    ,"yahooMobileRankIn"
                    ,"googleRankIn"
                    ,"googleMobileRankIn"
                    ,"bingRankIn"
                    ,"inquiryDataList"
                    ,"addType"
			};
		}
	};

	/**
	 * 帳票テンプレートにフィールドが存在するか確認します。
	 *
	 * @param field 確認するフィールド
	 * @return 存在する場合 true を返します
	 */
	protected boolean isProperty(String field) {
		return SearchRankProvider.isProperty(field);
	}
}

