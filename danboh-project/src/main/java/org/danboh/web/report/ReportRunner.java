/*
 * Danboh is a Source Code Generation program developed by BobTabo.
 * 
 * Copyright (c) 2009 BobTabo. All Rights Reserved.
 */
package org.danboh.web.report;

import java.io.InputStream;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import org.danboh.web.util.EnvSettingUtil;

/* $Id: ReportRunner.java,v 1.1 2010/06/25 08:18:52 nagashiba Exp $ */

/**
 * 帳票実行クラスです。
 * 
 * @author <a href="mailto:nagashiba@adv-co.com">Satoshi Nagashiba</a>
 * @version $Revision: 1.1 $ $Date: 2010/06/25 08:18:52 $
 */
public abstract class ReportRunner<E> implements ReportRunnable<E> {

	/** 作業ディレクトリであることを意味します */
	protected static final String USER_DIR = System.getProperty("user.dir");

	/** テンプレートフォルダパスであることを意味します */
	protected static final String TEMPLATE_DIR = EnvSettingUtil.getString("report.template.path");

	/** テンプレートファイルのリスト */
	private final List<String> _templateNames;

	/**
	 * コンストラクタ
	 * 
	 * @param templateName
	 *            テンプレートファイル名
	 */
	public ReportRunner(String... templateName) {
		_templateNames = new LinkedList<String>();
		_templateNames.addAll(Arrays.asList(templateName));
	}

	/**
	 * テンプレートを取得します。
	 * 
	 * @return テンプレートの入力ストリーム
	 */
	protected InputStream getTemplate() {
		return getTemplate(0);
	}

	/**
	 * テンプレートを取得します。
	 * 
	 * @param index
	 *            インデックス
	 * @return テンプレートの入力ストリーム
	 */
	protected InputStream getTemplate(int index) {
		return ReportRunner.class.getResourceAsStream(TEMPLATE_DIR + getTemplateName(index));
	}

	/**
	 * テンプレートファイル名を取得します。
	 * 
	 * @return テンプレートファイル名
	 */
	protected String getTemplateName() {
		return getTemplateName(0);
	}

	/**
	 * テンプレートファイル名を取得します。
	 * 
	 * @param index
	 *            インデックス
	 * @return テンプレートファイル名
	 */
	protected String getTemplateName(int index) {
		return _templateNames.get(index);
	}

	/**
	 * 出力先ディレクトリのパスを取得します。
	 * 
	 * @return 出力先ディレクトリのパス
	 */
	protected String getOutDirectoryPath() {
		return System.getProperty("user.home");
	}

	/**
	 * テンプレートファイル数を取得します。
	 * 
	 * @return テンプレートファイル数
	 */
	protected int getTemplateSize() {
		return _templateNames.size();
	}
}
