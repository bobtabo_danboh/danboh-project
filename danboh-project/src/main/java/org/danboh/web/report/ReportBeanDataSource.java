package org.danboh.web.report;

/* $Id: ReportBeanDataSource.java,v 1.1 2010/06/25 08:18:52 nagashiba Exp $ */

import net.sf.jasperreports.engine.data.JRAbstractBeanDataSource;

/**
 * 基底データソースクラスです。
 * 
 * @author <a href="mailto:nagashiba@adv-co.com">Satoshi Nagashiba</a>
 * @version $Revision: 1.1 $ $Date: 2010/06/25 08:18:52 $
 */
public abstract class ReportBeanDataSource extends JRAbstractBeanDataSource {

	/**
	 * コンストラクタ
	 */
	public ReportBeanDataSource() {
		this(true);
	}

	/**
	 * コンストラクタ
	 * 
	 * @param isUseFieldDescription
	 *            isUseFieldDescription
	 */
	public ReportBeanDataSource(boolean isUseFieldDescription) {
		super(isUseFieldDescription);
	}

	/**
	 * 帳票プロパティ処理を提供するインターフェースです。
	 */
	protected static interface ReportPropertyNameProvider {

		/**
		 * 対象プロパティが帳票内に存在するか確認します。
		 * 
		 * @param property
		 *            対象プロパティ
		 * @return 存在する場合は true を返します。
		 */
		public abstract boolean isProperty(String property);
	}

	/**
	 * 帳票プロパティ処理を実装したクラスです。
	 */
	protected static abstract class BaseReportPropertyNameProvider implements ReportPropertyNameProvider {

		/**
		 * {@inheritDoc}
		 */
		@Override
		public boolean isProperty(String property) {
			boolean result = false;
			String[] propertyNames = getPropertyNames();
			for (int i = 0; i < propertyNames.length; i++) {
				if (property.equals(propertyNames[i])) {
					result = true;
					break;
				}
			}
			return result;
		}

		/**
		 * プロパティ名を取得します。
		 * 
		 * @return プロパティ名
		 */
		protected abstract String[] getPropertyNames();
	}
}
