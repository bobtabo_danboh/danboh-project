/*
 * Danboh is a Source Code Generation program developed by BobTabo.
 * 
 * Copyright (c) 2009 BobTabo. All Rights Reserved.
 */
package org.danboh.web.report;

/* $Id: ReportRunnable.java,v 1.1 2010/06/25 08:18:52 nagashiba Exp $ */

/**
 * 帳票実行インターフェースです。
 * 
 * @author <a href="mailto:nagashiba@adv-co.com">Satoshi Nagashiba</a>
 * @version $Revision: 1.1 $ $Date: 2010/06/25 08:18:52 $
 */
public interface ReportRunnable<E> {

	/**
	 * 帳票処理を実行します。
	 * 
	 * @param dto
	 *            帳票DTO
	 * @return 生成したPDF出力データ
	 * @throws Exception
	 *             例外
	 */
	public byte[] run(E dto) throws Exception;
}
