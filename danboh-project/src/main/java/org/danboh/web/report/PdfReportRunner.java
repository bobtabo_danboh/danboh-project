/*
 * Danboh is a Source Code Generation program developed by BobTabo.
 * 
 * Copyright (c) 2009 BobTabo. All Rights Reserved.
 */
package org.danboh.web.report;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

import org.apache.commons.io.IOUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/* $Id: PdfReportRunner.java,v 1.1 2010/06/25 08:18:52 nagashiba Exp $ */

/**
 * PDF帳票実行クラスです。
 * 
 * @author <a href="mailto:nagashiba@adv-co.com">Satoshi Nagashiba</a>
 * @version $Revision: 1.1 $ $Date: 2010/06/25 08:18:52 $
 */
public abstract class PdfReportRunner<E> extends ReportRunner<E> {

	/** このクラスで発生した実行状況をログに記録するためのオブジェクトです。 */
	private static final Log logger = LogFactory.getLog(PdfReportRunner.class);

	/**
	 * コンストラクタ
	 * 
	 * @param templateName
	 *            テンプレートファイル名
	 */
	public PdfReportRunner(String... templateName) {
		super(templateName);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public byte[] run(E dto) throws Exception {
		byte[] result = null;
		InputStream[] stream = new ByteArrayInputStream[getTemplateSize()];
		try {
			for (int i = 0; i < stream.length; i++) {
				stream[i] = getTemplate(i);
			}
			result = execute(dto, stream);
		} catch (Exception e) {
			logger.error(e);
			throw e;
		} finally {
			for (int i = 0; i < stream.length; i++) {
				IOUtils.closeQuietly(stream[i]);
			}
		}
		return result;
	}

	/**
	 * 帳票作成を処理します。
	 * 
	 * @param jasper
	 *            jasperファイルの入力ストリーム
	 * @return 生成したPDF出力データ
	 * @throws Exception
	 *             例外
	 */
	protected abstract byte[] execute(E dto, InputStream... jasper) throws Exception;
}
