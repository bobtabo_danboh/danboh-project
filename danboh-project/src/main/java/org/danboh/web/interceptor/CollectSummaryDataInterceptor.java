package org.danboh.web.interceptor;


import java.util.ArrayList;
import java.util.List;

import org.seasar.framework.container.S2Container;
import org.seasar.framework.container.annotation.tiger.Binding;

import org.danboh.web.action.ImageViewAction;
import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.Interceptor;
import com.opensymphony.xwork2.interceptor.PreResultListener;

public class CollectSummaryDataInterceptor implements Interceptor {
	private static final long serialVersionUID = 399571306080257424L;
	@Binding
	public S2Container container;
	private static final String TARGET_NAMESPACE = "/admin";
	private static List<String> EXCLUDES_RESULT = new ArrayList<String>();
	static {
		EXCLUDES_RESULT.add("internalError");
	}
	@SuppressWarnings("unchecked")
	private static List<Class> EXCLUDES_ACTION = new ArrayList<Class>();
	static {
		EXCLUDES_ACTION.add(ImageViewAction.class);
	}
	/** クッキー格納キー. */
	private static final String KEY_COOKIE_USER_KEY = "userKey";

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String intercept(ActionInvocation invocation) throws Exception {
		if (invocation.getProxy().getNamespace().startsWith(TARGET_NAMESPACE)) {
			invocation.addPreResultListener(new PreResultListener() {
				public void beforeResult(ActionInvocation invocation, String result) {
					Object action = invocation.getAction();
					if (!EXCLUDES_RESULT.contains(result) && !EXCLUDES_ACTION.contains(action.getClass())) {

					}
				}
			});
		}
		return invocation.invoke();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void destroy() {
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void init() {
	}
}
