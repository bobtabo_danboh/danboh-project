package org.danboh.web.interceptor;


import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.ServletActionContext;
import org.seasar.framework.container.S2Container;
import org.seasar.framework.container.annotation.tiger.Binding;

import org.danboh.web.dto.SessionDataDto;
import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.Interceptor;
import com.opensymphony.xwork2.interceptor.PreResultListener;
import com.opensymphony.xwork2.util.ValueStack;

public class SetFrontCommonDataInterceptor implements Interceptor {
	private static final long serialVersionUID = 1875603280668779399L;
	@Binding
	public S2Container container;
	private static final String TARGET_NAMESPACE = "/admin";
	private static List<String> EXCLUDES_RESULT = new ArrayList<String>();
	static {
		EXCLUDES_RESULT.add("internalError");
	}
	@SuppressWarnings("unchecked")
	private static List<Class> EXCLUDES_ACTION = new ArrayList<Class>();
	static {
		EXCLUDES_ACTION.add(org.danboh.web.action.ImageViewAction.class);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String intercept(ActionInvocation invocation) throws Exception {
		if (invocation.getProxy().getNamespace().startsWith(TARGET_NAMESPACE)) {
			invocation.addPreResultListener(new PreResultListener() {
				public void beforeResult(ActionInvocation invocation, String result) {
					Object action = invocation.getAction();
					if (!EXCLUDES_RESULT.contains(result) && !EXCLUDES_ACTION.contains(action.getClass())) {
						SessionDataDto sessionDataDto = (SessionDataDto) container.getComponent(SessionDataDto.class);
						ValueStack stack = invocation.getInvocationContext().getValueStack();

						HttpServletRequest request = ServletActionContext.getRequest();
						String html = request.getRequestURI().replaceAll(request.getContextPath(), "").replaceAll(
								TARGET_NAMESPACE, "").replaceAll("/", "");

					}
				}
			});
		}
		return invocation.invoke();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void destroy() {
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void init() {
	}
}
