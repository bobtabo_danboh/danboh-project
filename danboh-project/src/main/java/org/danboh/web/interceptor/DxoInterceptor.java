package org.danboh.web.interceptor;

import org.aopalliance.intercept.MethodInvocation;
import org.dozer.Mapper;
import org.seasar.framework.aop.interceptors.AbstractInterceptor;
import org.seasar.framework.container.annotation.tiger.Binding;
import org.seasar.framework.util.MethodUtil;

/**
 * Dozerのマッピング実装を提供するInterceptorです。
 */
public class DxoInterceptor extends AbstractInterceptor {
	private static final long serialVersionUID = -4138366823418978608L;
	// Dozerのマッピング設定
	@Binding
	private Mapper mapper;

	/**
	 * Dozerのマッピングを行います。
	 * 
	 * @param methodinvocation
	 *            MethodInvocation
	 * @return なし
	 * @throws Throwable
	 *             実装クラスが送出する例外
	 */
	@Override
	public Object invoke(MethodInvocation methodinvocation) throws Throwable {
		if (methodinvocation.getMethod().getName().startsWith("mapping")) {
			if (methodinvocation.getArguments().length == 2) {
				mapper.map(methodinvocation.getArguments()[0], methodinvocation.getArguments()[1]);
			}
		}
		// 実装クラスがある場合それも実行
		if (!MethodUtil.isAbstract(methodinvocation.getMethod())) {
			return methodinvocation.proceed();
		}
		return null;
	}
}
