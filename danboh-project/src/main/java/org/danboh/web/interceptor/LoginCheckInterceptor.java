package org.danboh.web.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.seasar.framework.container.S2Container;
import org.seasar.framework.container.annotation.tiger.Binding;

import org.danboh.web.annotation.BeforeLogin;
import org.danboh.web.annotation.RequiredReLogin;
import org.danboh.web.annotation.RequiredSSL;
import org.danboh.web.dto.SessionDataDto;
import org.danboh.web.util.EnvSettingUtil;
import org.danboh.web.util.ServletUtil;
import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.Interceptor;

public class LoginCheckInterceptor implements Interceptor {
	private static final long serialVersionUID = -3986146485987150113L;
	@Binding
	public S2Container container;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String intercept(ActionInvocation invocation) throws Exception {
		Class<?> action = Class.forName(invocation.getProxy().getConfig().getClassName());

		HttpServletRequest httpServletRequest = (HttpServletRequest) container.getComponent(HttpServletRequest.class);
		HttpServletResponse httpServletResponse = (HttpServletResponse) container
				.getComponent(HttpServletResponse.class);

		SessionDataDto sessionDataDto = (SessionDataDto) container.getComponent(SessionDataDto.class);

		// SSLチェック
		RequiredSSL requiredSSL = action.getAnnotation(RequiredSSL.class);
		if (EnvSettingUtil.isTrue("useSSL")) {
			if (httpServletRequest.getScheme().equalsIgnoreCase("http") && requiredSSL != null) {
				// リダレクト
				String redirectURL = ServletUtil.createRedirectUrl(httpServletRequest, httpServletResponse, "http",
						"https");
				httpServletResponse.sendRedirect(redirectURL);

				return null;
			} else if (httpServletRequest.getScheme().equalsIgnoreCase("https") && requiredSSL == null) {
				// リダイレクト
				String redirectURL = ServletUtil.createRedirectUrl(httpServletRequest, httpServletResponse, "https",
						"http");
				httpServletResponse.sendRedirect(redirectURL);

				return null;
			}
		}

		boolean verifiedLogin = false;
		// 認証チェック
		// LoginType loginType = action.getAnnotation(LoginType.class);
		RequiredReLogin requiredReLogin = action.getAnnotation(RequiredReLogin.class);
		BeforeLogin beforeLogin = action.getAnnotation(BeforeLogin.class);

		if (requiredReLogin == null) {
			// 再ログインが必要な機能が終了した場合、認証をクリア
			sessionDataDto.isReLogin = false;
		}

		// if (loginType != null || requiredReLogin != null) {
		// // ログイン後ページを設定
		// sessionDataDto.afterLoginPath =
		// ServletUtil.createRedirectUrl(httpServletRequest,
		// httpServletResponse,
		// null, null);
		// if (loginType != null) {
		// // ログインが必要の場合
		// if (sessionDataDto == null || !sessionDataDto.isLogin()) {
		// return "login";
		// }
		//
		// // アカウント種別チェック
		// for (AccountType accountType : loginType.types()) {
		// if (accountType == sessionDataDto.accountType) {
		// verifiedLogin = true;
		// }
		// }
		// }
		//
		// if (requiredReLogin != null) {
		// // 再ログインが必要な場合
		// if (sessionDataDto.isReLogin) {
		// verifiedLogin = true;
		// } else {
		// return "login";
		// }
		// }
		// } else if (beforeLogin != null) {
		// // ログインが必要無い
		// verifiedLogin = true;
		// }

		if (verifiedLogin) {
			return invocation.invoke();
		}

		return "login";
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void destroy() {
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void init() {
	}
}
