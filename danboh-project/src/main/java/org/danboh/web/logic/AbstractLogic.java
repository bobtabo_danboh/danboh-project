package org.danboh.web.logic;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.seasar.extension.jdbc.AutoSelect;
import org.seasar.extension.jdbc.service.S2AbstractService;
import org.seasar.framework.container.S2Container;
import org.seasar.framework.container.annotation.tiger.Binding;

import org.danboh.web.dto.PagerCondition;
import org.danboh.web.exceptions.AppException;
import org.danboh.web.util.ImageProducer;

/**
 * サービスの抽象クラスです。
 *
 * @author S2JDBC-Gen
 * @param <T>
 *            エンティティの型
 */
public abstract class AbstractLogic<T> extends S2AbstractService<T> {
	private String[] DEFAULT_INCLUDES_FIELDS_FOR_UPDATE = new String[] { "updateDate", "updateLoginId", "version" };
	@Binding
	public S2Container container;
	public ImageProducer imageProducer;

	public AutoSelect<T> select() {
		return jdbcManager.from(entityClass);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int insert(T entity) {
		prepareInsert(entity);
		int result = jdbcManager.insert(entity).excludes("searchIndex").execute();
		exportFile(entity);
		return result;
	}

	/**
	 * 共通フィールドを設定しないで登録します。
	 *
	 * @param entity
	 *            エンティティ
	 * @return 登録件数
	 */
	public int insertNoPrepare(T entity) {
		int result = jdbcManager.insert(entity).excludes("searchIndex").execute();
		exportFile(entity);
		return result;
	}

	public int insertExcludesNull(T entity) {
		prepareInsert(entity);
		int result = jdbcManager.insert(entity).excludesNull().excludes("searchIndex").execute();
		exportFile(entity);
		return result;
	}

	/**
	 * 共通フィールドを設定しないで、null値のプロパティを挿入対象から除外した登録を行います。
	 *
	 * @param entity
	 *            エンティティ
	 * @return 登録件数
	 */
	public int insertExcludesNullNoPrepare(T entity) {
		int result = jdbcManager.insert(entity).excludesNull().excludes("searchIndex").execute();
		exportFile(entity);
		return result;
	}

	/**
	 * 登録時にファイル出力を行わない登録処理です。
	 *
	 * @param entity
	 *            エンティティ
	 * @return 登録数
	 */
	public int insertExcludesNullNoExportFile(T entity) {
		prepareInsert(entity);
		int result = jdbcManager.insert(entity).excludesNull().excludes("searchIndex").execute();
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int update(T entity) {
		prepareUpdate(entity);
		int result = jdbcManager.update(entity).excludes("searchIndex").execute();
		exportFile(entity);
		return result;
	}

	public int updateExcludes(T entity, String... deniedFields) {
		prepareUpdate(entity);
		int result = jdbcManager.update(entity).excludes(deniedFields).excludes("searchIndex").excludesNull().execute();
		exportFile(entity);
		return result;
	}

	public int updateExcludesNull(T entity) {
		prepareUpdate(entity);
		int result = jdbcManager.update(entity).excludes("searchIndex").excludesNull().execute();
		exportFile(entity);
		return result;
	}

	/**
	 * 共通フィールドを設定しないで、null値のプロパティを挿入対象から除外した更新を行います。
	 *
	 * @param entity
	 *            エンティティ
	 * @return 更新件数
	 */
	public int updateExcludesNullNoPrepare(T entity) {
		int result = jdbcManager.update(entity).excludes("searchIndex").excludesNull().execute();
		exportFile(entity);
		return result;
	}

	public int updateIncludes(T entity, String... allowedFields) {
		prepareUpdate(entity);
		int result = jdbcManager.update(entity).includes(DEFAULT_INCLUDES_FIELDS_FOR_UPDATE).includes(allowedFields)
				.execute();
		exportFile(entity);
		return result;
	}

	/**
	 * 更新時にファイル出力を行わない更新処理です。
	 *
	 * @param entity
	 *            エンティティ
	 * @return 自動更新
	 */
	public int updateExcludesNullNoExportFile(T entity) {
		prepareUpdate(entity);
		int result = jdbcManager.update(entity).excludes("searchIndex").excludesNull().execute();
		return result;
	}

	/**
	 * 更新時にファイル出力を行わない更新処理です。
	 *
	 * @param entity
	 *            エンティティ
	 * @param allowedFields
	 *            更新するフィールド名
	 * @return 自動更新
	 */
	public int updateIncludesNoExportFile(T entity, String... allowedFields) {
		prepareUpdate(entity);
		int result = jdbcManager.update(entity).includes(DEFAULT_INCLUDES_FIELDS_FOR_UPDATE).includes(allowedFields)
				.execute();
		return result;
	}

	/**
	 * 論理削除を行います。
	 *
	 * @param entity
	 *            エンティティ
	 * @return 削除した数
	 */
	public int logicalDelete(T entity) {
		// 論理削除
		try {
			// 共通のフィールドに値を入れる
			if (PropertyUtils.getPropertyDescriptor(entity, "deleteDate") != null) {
				String loginId = getLoginId();
				Timestamp now = new Timestamp(System.currentTimeMillis());
				PropertyUtils.setProperty(entity, "deleteDate", now);
				PropertyUtils.setProperty(entity, "deleteLoginId", loginId);
				PropertyUtils.setProperty(entity, "deleteFlag", true);
			}
		} catch (IllegalAccessException e) {
			throw new AppException(e);
		} catch (InvocationTargetException e) {
			throw new AppException(e);
		} catch (NoSuchMethodException e) {
			throw new AppException(e);
		}
		return super.update(entity);
	}

	public AutoSelect<T> paging(PagerCondition pagerCondition, AutoSelect<T> autoSelect,
			AutoSelect<T> autoSelectForCount) {
		pagerCondition.setCount((int) autoSelectForCount.getCount());
		return autoSelect.orderBy(pagerCondition.getOrderBy()).offset(pagerCondition.getOffset())
				.limit(pagerCondition.getLimit());
	}

	public T selectById(int id) {
		return select().id(id).getSingleResult();
	}

	public <E> AutoSelect<E> eagerAll(AutoSelect<E> autoSelect) {
		return eagerAll(autoSelect, null);
	}

	public <E> AutoSelect<E> eagerAll(AutoSelect<E> autoSelect, String prefix) {
		List<String> lazyFields = new ArrayList<String>();
		Field[] fields = entityClass.getFields();
		for (Field field : fields) {
			if (field.getType() == byte[].class) {
				if (prefix != null) {
					lazyFields.add(prefix + "." + field.getName());
				} else {
					lazyFields.add(field.getName());
				}
			}
		}

		return autoSelect.eager(lazyFields.toArray(new String[0]));
	}

	protected T prepareInsert(T entity) {
		try {
			// 共通のフィールドに値を入れる
			if (PropertyUtils.getPropertyDescriptor(entity, "createDate") != null) {
				String loginId = getLoginId();
				Timestamp now = new Timestamp(System.currentTimeMillis());
				PropertyUtils.setProperty(entity, "createDate", now);
				PropertyUtils.setProperty(entity, "updateDate", now);
				PropertyUtils.setProperty(entity, "createLoginId", loginId);
				PropertyUtils.setProperty(entity, "updateLoginId", loginId);
				PropertyUtils.setProperty(entity, "deleteFlag", false);
			}
		} catch (IllegalAccessException e) {
			throw new AppException(e);
		} catch (InvocationTargetException e) {
			throw new AppException(e);
		} catch (NoSuchMethodException e) {
			throw new AppException(e);
		}
		return entity;
	}

	private T prepareUpdate(T entity) {
		try {
			// 共通のフィールドに値を入れる
			if (PropertyUtils.getPropertyDescriptor(entity, "updateDate") != null) {
				String loginId = getLoginId();
				Timestamp now = new Timestamp(System.currentTimeMillis());
				PropertyUtils.setProperty(entity, "updateDate", now);
				PropertyUtils.setProperty(entity, "updateLoginId", loginId);
			}
		} catch (IllegalAccessException e) {
			throw new AppException(e);
		} catch (InvocationTargetException e) {
			throw new AppException(e);
		} catch (NoSuchMethodException e) {
			throw new AppException(e);
		}

		return entity;
	}

	private void exportFile(T entity) {
		String[] imageSavePath = imageProducer.getImageSavePath(entity);
		if (imageSavePath != null) {
			try {
				FileUtils.forceMkdir(new File(imageSavePath[0]));
				FileUtils.forceMkdir(new File(imageSavePath[1]));
			} catch (IOException e) {
				throw new AppException(e);
			}
			exportFile(entity, "headerImage", imageSavePath[0], ImageProducer.contentTypeExtentionMap);
		}
	}

	private void exportFile(T entity, String fieldName, String path, Map<String, String> contentTypeExtentionMap) {
		try {
			if (PropertyUtils.getPropertyDescriptor(entity, fieldName) != null) {
				String contentType = (String) PropertyUtils.getProperty(entity, fieldName + "ContentType");
				byte[] content = (byte[]) PropertyUtils.getProperty(entity, fieldName);
				if (content != null && content.length > 0) {
					String ext = contentTypeExtentionMap.get(contentType);
					FileOutputStream out = new FileOutputStream(path + fieldName + ext);
					IOUtils.copy(new ByteArrayInputStream(content), out);
					out.flush();
					out.close();
				}
			}
		} catch (IllegalAccessException e) {
			throw new AppException(e);
		} catch (InvocationTargetException e) {
			throw new AppException(e);
		} catch (NoSuchMethodException e) {
			throw new AppException(e);
		} catch (IOException e) {
			throw new AppException(e);
		}
	}

	private String getLoginId() {
		StringBuffer sb = new StringBuffer();
		String format = "%010d";
		String path = container.getPath();
		// if ("app.dicon".equals(path)) {
		// SessionDataDto sessionDataDto = (SessionDataDto)
		// container.getComponent(SessionDataDto.class);
		// if (sessionDataDto.accountType == AccountType.ADMIN) {
		// sb.append("ADM");
		// sb.append(String.format(format, sessionDataDto.manager.getId()));
		// } else if (sessionDataDto.accountType == AccountType.CLIENT) {
		// sb.append("CLT");
		// Integer id = 0;
		// if (sessionDataDto.client != null && sessionDataDto.client.getId() !=
		// null) {
		// id = sessionDataDto.client.getId();
		// }
		// sb.append(String.format(format, id));
		// } else if (sessionDataDto.accountType == AccountType.USER) {
		// sb.append("USR");
		// Integer id = 0;
		// if (sessionDataDto.member != null && sessionDataDto.member.getId() !=
		// null) {
		// id = sessionDataDto.member.getId();
		// }
		// sb.append(String.format(format, id));
		// } else {
		// sb.append("NON");
		// sb.append(String.format(format, 0));
		// }
		// } else if ("app-task.dicon".equals(path)) {
		// sb.append("NON");
		// sb.append(String.format(format, 0));
		// }

		return sb.toString();
	}
}