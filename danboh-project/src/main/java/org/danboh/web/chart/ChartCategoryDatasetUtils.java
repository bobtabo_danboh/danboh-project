/*
 * Danboh is a Source Code Generation program developed by BobTabo.
 * 
 * Copyright (c) 2009 BobTabo. All Rights Reserved.
 */
package org.danboh.web.chart;

import java.awt.Font;
import java.text.NumberFormat;

import org.apache.commons.lang.StringUtils;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.CategoryAxis;
import org.jfree.chart.axis.CategoryLabelPositions;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.labels.StandardCategoryItemLabelGenerator;
import org.jfree.chart.labels.StandardCategoryToolTipGenerator;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.DatasetRenderingOrder;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.category.BarRenderer;
import org.jfree.chart.renderer.category.CategoryItemRenderer;
import org.jfree.chart.renderer.category.LineAndShapeRenderer;
import org.jfree.chart.title.LegendTitle;
import org.jfree.data.category.CategoryDataset;

/* $Id: ChartCategoryDatasetUtils.java,v 1.1 2010/06/25 08:18:52 nagashiba Exp $ */

/**
 * チャート関連（カテゴリデータセット用）のユーティリティクラスです。
 * 
 * @author <a href="mailto:nagashiba@adv-co.com">Satoshi Nagashiba</a>
 * @version $Revision: 1.1 $ $Date: 2010/06/25 08:18:52 $
 */
public final class ChartCategoryDatasetUtils {

	/** デフォルトフォント */
	private static final Font DEFAULT_FONT = ChartUtils.DEFAULT_FONT;

	/**
	 * コンストラクタ（外部からのインスタンス生成を許可しない）
	 */
	private ChartCategoryDatasetUtils() {
	}

	/**
	 * JFreeChartオブジェクトを生成します。
	 * 
	 * @param model
	 *            チャートモデル
	 * @return JFreeChartオブジェクト
	 */
	protected static JFreeChart getInstance(ChartModel model) {
		JFreeChart result = null;

		// 棒グラフ
		if (ChartModel.BAR_CHART.equals(model.getType())) {
			result = ChartFactory.createBarChart(StringUtils.EMPTY, model.getBottomLabel(), model.getLeftLabel(), model
					.getCategoryDataset()[0], model.getPlotOrientation(), false, false, false);
		}
		// 折れ線グラフ
		else if (ChartModel.LINE_CHART.equals(model.getType())) {
			result = ChartFactory.createLineChart(StringUtils.EMPTY, model.getBottomLabel(), model.getLeftLabel(),
					model.getCategoryDataset()[0], model.getPlotOrientation(), false, false, false);
		}
		// 折れ線棒グラフ
		else if (ChartModel.LINE_BAR_CHART.equals(model.getType())) {
			result = ChartFactory.createLineChart(StringUtils.EMPTY, model.getBottomLabel(), model.getLeftLabel(),
					model.getCategoryDataset()[0], model.getPlotOrientation(), true, false, false);
		}

		return result;
	}

	/**
	 * 棒を描画します。
	 * 
	 * @param chart
	 *            チャート
	 * @param model
	 *            チャートモデル
	 */
	protected static void setBarRenderer(JFreeChart chart, ChartModel model) {
		CategoryPlot plot = chart.getCategoryPlot();

		// 棒の色
		CategoryItemRenderer renderer = null;

		// 棒グラフ
		if (ChartModel.BAR_CHART.equals(model.getType())) {
			renderer = plot.getRenderer();
			renderer.setSeriesPaint(0, model.getColor());
		}
		// 折れ線棒グラフ
		else {
			renderer = plot.getRenderer(LineBarChartModel.BAR);
			renderer.setSeriesPaint(0, model.getColor(LineBarChartModel.BAR));
		}

		// 棒の幅
		BarRenderer barRenderer = null;

		// 棒グラフ
		if (ChartModel.BAR_CHART.equals(model.getType())) {
			barRenderer = (BarRenderer) plot.getRenderer();
		}
		// 折れ線棒グラフ
		else {
			barRenderer = (BarRenderer) plot.getRenderer(LineBarChartModel.BAR);
		}
		barRenderer.setItemMargin(0.03D);
		barRenderer.setMaximumBarWidth(0.05D);
		barRenderer.setShadowVisible(false);
	}

	/**
	 * 棒グラフを設定します。
	 * 
	 * @param chart
	 *            チャート
	 * @param model
	 *            チャートモデル
	 * @return 棒グラフチャート
	 */
	protected static void setBarChart(JFreeChart chart, ChartModel model) {
		CategoryPlot plot = chart.getCategoryPlot();

		// 縦棒グラフの場合
		if (PlotOrientation.VERTICAL.equals(model.getPlotOrientation())) {
			CategoryAxis axis = plot.getDomainAxis();
			axis.setCategoryLabelPositions(CategoryLabelPositions.createUpRotationLabelPositions(0.8D));

			BarRenderer barrenderer = (BarRenderer) plot.getRenderer();
			barrenderer.setItemLabelAnchorOffset(9D);
			barrenderer.setBaseToolTipGenerator(new StandardCategoryToolTipGenerator());
		}
	}

	/**
	 * 折れ線グラフを設定します。
	 * 
	 * @param chart
	 *            チャート
	 * @param model
	 *            チャートモデル
	 * @return 折れ線グラフチャート
	 */
	protected static void setLineChart(JFreeChart chart, ChartModel model) {
		// 線の描画
		CategoryPlot plot = chart.getCategoryPlot();
		LineAndShapeRenderer lineRenderer = (LineAndShapeRenderer) plot.getRenderer();
		lineRenderer.setSeriesPaint(0, model.getColor());
		lineRenderer.setBaseShapesVisible(model.getChartLineMarker());
		lineRenderer.setDrawOutlines(true);
		// 値の表示
		if (model.getChartLineLabel()) {
			lineRenderer.setBaseItemLabelGenerator(new StandardCategoryItemLabelGenerator());
			lineRenderer.setBaseItemLabelsVisible(true);
		}
		chart.addLegend(new LegendTitle(plot));
	}

	/**
	 * 折れ線棒グラフを設定します。
	 * 
	 * @param chart
	 *            チャート
	 * @param model
	 *            チャートモデル
	 * @return 折れ線縦棒グラフチャート
	 */
	protected static void setLineBarChart(JFreeChart chart, LineBarChartModel model) {
		CategoryPlot plot = chart.getCategoryPlot();

		// 線の描画
		LineAndShapeRenderer lineRenderer = (LineAndShapeRenderer) plot.getRenderer();
		lineRenderer.setItemLabelAnchorOffset(9D);
		lineRenderer.setBaseToolTipGenerator(new StandardCategoryToolTipGenerator());
		lineRenderer.setSeriesPaint(LineBarChartModel.LINE, model.getColor(LineBarChartModel.LINE));
		lineRenderer.setBaseShapesVisible(true);
		lineRenderer.setDrawOutlines(true);
		lineRenderer.setSeriesShape(0, new java.awt.geom.Ellipse2D.Double(-3D, -3D, 6D, 6D));

		// 棒グラフ設定
		plot.setDataset(LineBarChartModel.BAR, model.getCategoryDataset()[LineBarChartModel.BAR]);
		plot.mapDatasetToRangeAxis(LineBarChartModel.LINE, LineBarChartModel.LINE);
		plot.mapDatasetToRangeAxis(LineBarChartModel.BAR, LineBarChartModel.BAR);
		plot.setDatasetRenderingOrder(DatasetRenderingOrder.REVERSE);

		// 棒の描画
		BarRenderer barrenderer = new BarRenderer();
		barrenderer.setSeriesPaint(LineBarChartModel.BAR, model.getColor(LineBarChartModel.BAR));
		plot.setRenderer(LineBarChartModel.BAR, barrenderer);

		// 横軸の設定
		CategoryAxis axis = plot.getDomainAxis();
		axis.setLowerMargin(0.02D);
		axis.setUpperMargin(0.02D);
		axis.setCategoryLabelPositions(CategoryLabelPositions.createUpRotationLabelPositions(0.8D));

		// 縦軸（右）の設定
		NumberAxis rangeAxis = new NumberAxis(model.getRightLabel());
		if (model.isDefaultRightRange()) {
			rangeAxis.setRange(ChartUtils.DEFAULT_RANGE);
		} else {
			setAxisRangeColumn(rangeAxis, model.getCategoryDataset()[LineBarChartModel.BAR]);
		}
		rangeAxis.setTickLabelFont(DEFAULT_FONT);
		rangeAxis.setLabelFont(DEFAULT_FONT);
		rangeAxis.setNumberFormatOverride(NumberFormat.getIntegerInstance());
		rangeAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());
		plot.setRangeAxis(LineBarChartModel.BAR, rangeAxis);
	}

	/**
	 * 縦軸ラベルを設定します
	 * 
	 * @param axis
	 * @param dataset
	 */
	protected static void setAxisRangeColumn(NumberAxis axis, CategoryDataset dataset) {
		double value = 0D;
		for (int i = 0; i < dataset.getColumnCount(); i++) {
			Number number = dataset.getValue(0, i);
			value += (number == null) ? 0 : number.doubleValue();
		}
		if (value == 0D) {
			axis.setRange(ChartUtils.DEFAULT_RANGE);
		} else {
			axis.setAutoRange(true);
		}
	}
}
