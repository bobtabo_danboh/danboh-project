/*
 * Danboh is a Source Code Generation program developed by BobTabo.
 * 
 * Copyright (c) 2009 BobTabo. All Rights Reserved.
 */
package org.danboh.web.chart;

/* $Id: LineBarChartModel.java,v 1.1 2010/06/25 08:18:52 nagashiba Exp $ */

/**
 * 折れ線グラフチャートモデル.
 * 
 * @author <a href="mailto:nagashiba@adv-co.com">Satoshi Nagashiba</a>
 * @version $Revision: 1.1 $ $Date: 2010/06/25 08:18:52 $
 */
public class LineBarChartModel extends ChartModel {

	/** シリアルバージョンID. */
	private static final long serialVersionUID = -8238821287373296505L;

	/** 折れ線であることを意味します */
	public static final int LINE = 0;

	/** 棒であることを意味します */
	public static final int BAR = 1;

	/** 日付の間引き間隔 */
	private int dateAxisRange;

	/**
	 * コンストラクタ
	 */
	public LineBarChartModel() {
		super();
		setType(LINE_BAR_CHART);
	}

	/**
	 * @return dateAxisRange
	 */
	public int getDateAxisRange() {
		return dateAxisRange;
	}

	/**
	 * @param dateAxisRange
	 *            セットする dateAxisRange
	 */
	public void setDateAxisRange(int dateAxisRange) {
		this.dateAxisRange = dateAxisRange;
	}
}
