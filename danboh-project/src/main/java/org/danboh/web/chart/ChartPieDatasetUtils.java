/*
 * Danboh is a Source Code Generation program developed by BobTabo.
 * 
 * Copyright (c) 2009 BobTabo. All Rights Reserved.
 */
package org.danboh.web.chart;

import java.awt.Color;
import java.awt.Font;

import org.apache.commons.lang.StringUtils;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.labels.StandardPieSectionLabelGenerator;
import org.jfree.chart.plot.RingPlot;
import org.jfree.ui.RectangleInsets;

/* $Id: ChartPieDatasetUtils.java,v 1.1 2010/06/25 08:18:52 nagashiba Exp $ */

/**
 * チャート関連（パイデータセット用）のユーティリティクラスです。
 * 
 * @author <a href="mailto:nagashiba@adv-co.com">Satoshi Nagashiba</a>
 * @version $Revision: 1.1 $ $Date: 2010/06/25 08:18:52 $
 */
public final class ChartPieDatasetUtils {

	/** デフォルトフォント */
	private static final Font DEFAULT_FONT = ChartUtils.DEFAULT_FONT;

	/** データなしメッセージ */
	private static final String NO_DATA_MESSAGE = ChartUtils.NO_DATA_MESSAGE;

	/** データなしメッセージ */
	private static final Font CHART_LABEL_FONT = ChartUtils.CHART_LABEL_FONT;

	/**
	 * コンストラクタ（外部からのインスタンス生成を許可しない）
	 */
	private ChartPieDatasetUtils() {
	}

	/**
	 * JFreeChartオブジェクトを生成します。
	 * 
	 * @param model
	 *            チャートモデル
	 * @return JFreeChartオブジェクト
	 */
	protected static JFreeChart getInstance(ChartModel model) {
		return ChartFactory.createRingChart(StringUtils.EMPTY, model.getPieDataset(), false, false, false);
	}

	/**
	 * ドーナツグラフを設定します。
	 * 
	 * @param chart
	 *            チャート
	 * @param model
	 *            チャートモデル
	 * @return ドーナツグラフチャート
	 */
	@SuppressWarnings("deprecation")
	protected static void setRingChart(JFreeChart chart, ChartModel model) {
		RingPlot plot = (RingPlot) chart.getPlot();

		// 背景画像の設定
		chart.setBackgroundPaint(new Color(255, 255, 255, 0));
		plot.setBackgroundPaint(new Color(255, 255, 255, 0));

		plot.setNoDataMessage(NO_DATA_MESSAGE);
		plot.setNoDataMessageFont(DEFAULT_FONT);
		plot.setSectionDepth(0.7D);
		plot.setCircular(true);
		plot.setOutlineVisible(false);

		// セクション色
		for (Integer index : model.getPieSectionIndex()) {
			plot.setSectionPaint(index.intValue(), model.getColor(index.intValue()));
			plot.setSectionOutlinePaint(index.intValue(), model.getColor(index.intValue()));
		}

		// セクションセパレータ非表示
		plot.setSeparatorsVisible(false);

		// ラベル設定
		plot.setLabelFont(CHART_LABEL_FONT);
		plot.setLabelGap(0.02D);
		plot.setLabelGenerator(new StandardPieSectionLabelGenerator("{2}"));
		plot.setLabelBackgroundPaint(new Color(255, 255, 255, 0));
		plot.setLegendLabelToolTipGenerator(new StandardPieSectionLabelGenerator());
		plot.setSimpleLabels(true);
		plot.setInteriorGap(0.0D);
		plot.setLabelOutlinePaint(new Color(255, 255, 255, 0));
		plot.setLabelPadding(RectangleInsets.ZERO_INSETS);
		plot.setLabelPaint(Color.WHITE);
		plot.setLabelLinkPaint(new Color(255, 255, 255, 0));

		// シャドウ設定
		plot.setShadowXOffset(0D);
		plot.setShadowYOffset(0D);
		plot.setLabelShadowPaint(null);
	}
}
