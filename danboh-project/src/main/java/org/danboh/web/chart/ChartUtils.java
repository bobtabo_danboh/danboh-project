/*
 * Danboh is a Source Code Generation program developed by BobTabo.
 * 
 * Copyright (c) 2009 BobTabo. All Rights Reserved.
 */
package org.danboh.web.chart;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.util.ResourceBundle;

import org.apache.commons.lang.StringUtils;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.CategoryAxis;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.title.TextTitle;
import org.jfree.data.Range;
import org.jfree.ui.RectangleEdge;
import org.jfree.ui.RectangleInsets;

/* $Id: ChartUtils.java,v 1.5 2010/07/28 08:36:52 minagawa Exp $ */

/**
 * チャート関連のユーティリティクラスです。
 * 
 * @author <a href="mailto:nagashiba@adv-co.com">Satoshi Nagashiba</a>
 * @version $Revision: 1.5 $ $Date: 2010/07/28 08:36:52 $
 */
public final class ChartUtils {

	/** デフォルトフォント */
	protected static final Font DEFAULT_FONT = new Font("MS UI Gothic", Font.PLAIN, 12);
	protected static final Font DEFAULT_FONT_CHART = new Font("MS UI Gothic", Font.PLAIN, 10);

	/** データなしメッセージ */
	protected static final String NO_DATA_MESSAGE = ResourceBundle.getBundle("chart").getString("chart.no.data");

	/** データなしメッセージ */
	protected static final Font CHART_LABEL_FONT = new Font("MS UI Gothic", Font.BOLD, 14);

	/** デフォルト範囲 */
	protected static final Range DEFAULT_RANGE = new Range(1D, 300D);

	/** グラフ目盛り */
	public static final int GRAPH_AMBIT = 300;

	/**
	 * コンストラクタ（外部からのインスタンス生成を許可しない）
	 */
	private ChartUtils() {
	}

	/**
	 * グラフを作成します。
	 * 
	 * @param model
	 *            チャートモデル
	 * @return 棒グラフチャート
	 */
	public static JFreeChart createChart(ChartModel model) {

		JFreeChart result = null;

		// JFreeChartオブジェクトの生成
		if (model.isCategory()) {
			result = ChartCategoryDatasetUtils.getInstance(model);
		} else {
			if (ChartModel.LINE_BAR_CHART.equals(model.getType())) {
				result = ChartXYDatasetUtils.getInstance(model);
			} else if (ChartModel.RING_CHART.equals(model.getType())) {
				result = ChartPieDatasetUtils.getInstance(model);
			}
		}

		// グラフ全体の設定
		result.setBackgroundPaint(Color.WHITE);
		result.setBorderVisible(false);
		result.setPadding(new RectangleInsets(5D, 5D, 5D, 5D));

		// タイトルの設定
		if (StringUtils.isNotEmpty(model.getTitle())) {
			TextTitle title = new TextTitle(model.getTitle());
			title.setFont(DEFAULT_FONT);
			title.setHorizontalAlignment(model.getTitleHorizontalAlignment());
			result.setTitle(title);
		}

		// 折れ線棒グラフの設定
		if (ChartModel.LINE_BAR_CHART.equals(model.getType())) {
			result.getLegend().setItemFont(DEFAULT_FONT);
			result.getLegend().setPosition(RectangleEdge.TOP);
		}

		// ドーナツグラフ以外の場合
		if (!ChartModel.RING_CHART.equals(model.getType())) {
			// グラフ全体の設定
			result.setBorderPaint(Color.WHITE);
			result.setBorderStroke(new BasicStroke(1));

			if (model.isCategory()) {
				// 描画領域の設定
				CategoryPlot plot = result.getCategoryPlot();
				plot.setBackgroundPaint(Color.WHITE);
				plot.setRangeGridlinePaint(Color.BLUE);

				// 横軸の設定
				CategoryAxis axis = plot.getDomainAxis();
				axis.setTickLabelsVisible(true);
				axis.setTickLabelFont(DEFAULT_FONT_CHART);
				axis.setLabelFont(DEFAULT_FONT_CHART);

				// 縦軸の設定
				NumberAxis rangeAxis = (NumberAxis) plot.getRangeAxis();
				rangeAxis.setTickLabelFont(DEFAULT_FONT_CHART);
				rangeAxis.setLabelFont(DEFAULT_FONT_CHART);
				rangeAxis.setInverted(model.isInverted());
				rangeAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());

				// 縦軸目盛の設定
				if (model.isDefaultLeftRange()) {
					rangeAxis.setRange(ChartUtils.DEFAULT_RANGE);
				} else {
					// ChartCategoryDatasetUtils.setAxisRangeColumn(rangeAxis,
					// model.getCategoryDataset()[0]);
					if (model.getLeftRange() == null) {
						rangeAxis.setRange(ChartUtils.DEFAULT_RANGE);
					} else {
						rangeAxis.setRange(model.getLeftRange());
					}
				}
			}
		}

		// 各種チャート設定
		if (ChartModel.BAR_CHART.equals(model.getType())) {
			ChartCategoryDatasetUtils.setBarChart(result, model);
		} else if (ChartModel.LINE_CHART.equals(model.getType())) {
			ChartCategoryDatasetUtils.setLineChart(result, model);
		} else if (LineBarChartModel.LINE_BAR_CHART.equals(model.getType())) {
			if (model.isCategory()) {
				ChartCategoryDatasetUtils.setLineBarChart(result, (LineBarChartModel) model);
			} else {
				ChartXYDatasetUtils.setXYLineBarChart(result, (LineBarChartModel) model);
			}
		} else if (ChartModel.RING_CHART.equals(model.getType())) {
			ChartPieDatasetUtils.setRingChart(result, model);
		}

		// 棒グラフの設定
		if ((ChartModel.BAR_CHART.equals(model.getType())) || (ChartModel.LINE_BAR_CHART.equals(model.getType()))) {
			if (model.isCategory()) {
				ChartCategoryDatasetUtils.setBarRenderer(result, model);
			}
		}

		// 折れ線グラフの設定
		if (ChartModel.LINE_CHART.equals(model.getType())) {
			result.getLegend().setItemFont(DEFAULT_FONT);
			result.getLegend().setPosition(RectangleEdge.BOTTOM);
		}

		return result;
	}

	/**
	 * チャート色を取得します。
	 * 
	 * @param model
	 *            チャートモデル
	 * @return チャート色
	 */
	public static Color[] getColor(ChartModel model) {
		String[] keys = null;

		if (ChartModel.BAR_CHART.equals(model.getType())) {
			keys = new String[] { "chart.bar.color" };
		} else if (ChartModel.LINE_CHART.equals(model.getType())) {
			keys = new String[] { "chart.line.color" };
		} else if (ChartModel.LINE_BAR_CHART.equals(model.getType())) {
			keys = new String[] { "chart.line.color", "chart.bar.color" };
		} else if (ChartModel.RING_CHART.equals(model.getType())) {
			keys = new String[] { "chart.ring.color1", "chart.ring.color2", "chart.ring.color3", "chart.ring.color4" };
		}

		Color[] result = new Color[keys.length];

		ResourceBundle bundle = ResourceBundle.getBundle("chart");

		for (int i = 0; i < keys.length; i++) {
			String value = bundle.getString(keys[i]);
			String[] values = value.split(",");
			result[i] = new Color(Integer.parseInt(values[0]), Integer.parseInt(values[1]), Integer.parseInt(values[2]));
		}

		return result;
	}
}
