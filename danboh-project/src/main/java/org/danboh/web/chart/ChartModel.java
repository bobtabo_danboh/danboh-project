/*
 * Danboh is a Source Code Generation program developed by BobTabo.
 * 
 * Copyright (c) 2009 BobTabo. All Rights Reserved.
 */
package org.danboh.web.chart;

import java.awt.Color;
import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.lang.ObjectUtils;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.Range;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.general.Dataset;
import org.jfree.data.general.PieDataset;
import org.jfree.data.xy.XYDataset;
import org.jfree.ui.HorizontalAlignment;

/* $Id: ChartModel.java,v 1.2 2010/07/21 09:50:52 minagawa Exp $ */

/**
 * チャートモデル.
 * 
 * @author <a href="mailto:nagashiba@adv-co.com">Satoshi Nagashiba</a>
 * @version $Revision: 1.2 $ $Date: 2010/07/21 09:50:52 $
 */
public class ChartModel implements Serializable {

	/** シリアルバージョンID. */
	private static final long serialVersionUID = 4435054829127327122L;

	/** 棒グラフであることを意味します */
	public static final String BAR_CHART = "bar";

	/** 折れ線グラフであることを意味します */
	public static final String LINE_CHART = "line";

	/** 折れ線棒グラフであることを意味します */
	public static final String LINE_BAR_CHART = "line_bar";

	/** ドーナツグラフであることを意味します */
	public static final String RING_CHART = "ring";

	/** チャート種類 */
	private String type;

	/** チャートの向き */
	private PlotOrientation plotOrientation;

	/** データセット */
	private Dataset[] dataset;

	/** チャート色 */
	private Color[] colors;

	/** 横軸（下）のラベル */
	private String bottomLabel;

	/** 縦軸（左）のラベル */
	private String leftLabel;

	/** 縦軸（右）のラベル */
	private String rightLabel;

	/** 問題選択肢のインデックスリスト */
	private List<Integer> pieSectionIndex;

	/** 縦目盛を算出せずデフォルト目盛を使用する場合のフラグ（左） */
	private boolean defaultLeftRange;

	/** 縦目盛を算出せずデフォルト目盛を使用する場合のフラグ（右） */
	private boolean defaultRightRange;

	private Range leftRange;

	private String title;

	private HorizontalAlignment titleHorizontalAlignment;

	/** グラフのライン上に値を表示する */
	private boolean chartLineLabel;

	/** グラフのライン上にマーカーを表示する */
	private boolean chartLineMarker;

	/** グラフ横軸の目盛りRange反転 */
	private boolean inverted;

	/**
	 * @return inverted
	 */
	public boolean isInverted() {
		return inverted;
	}

	/**
	 * @param inverted
	 *            セットする inverted
	 */
	public void setInverted(boolean inverted) {
		this.inverted = inverted;
	}

	/**
	 * コンストラクタ
	 */
	public ChartModel() {
		setType(BAR_CHART);
		setPlotOrientation(PlotOrientation.VERTICAL);
		setPieSectionIndex(new LinkedList<Integer>());
	}

	/**
	 * コンストラクタ
	 * 
	 * @param type
	 *            チャート種類
	 */
	public ChartModel(String type) {
		this();
		setType(type);
	}

	/**
	 * @return type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type
	 *            セットする type
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * @return orientation
	 */
	public PlotOrientation getPlotOrientation() {
		return plotOrientation;
	}

	/**
	 * @param orientation
	 *            セットする orientation
	 */
	public void setPlotOrientation(PlotOrientation orientation) {
		this.plotOrientation = orientation;
	}

	/**
	 * @return dataset
	 */
	public Dataset[] getDataset() {
		return dataset;
	}

	/**
	 * @param dataset
	 *            セットする dataset
	 */
	public void setDataset(Dataset... dataset) {
		this.dataset = dataset;
	}

	/**
	 * @return categoryDataset
	 */
	public CategoryDataset[] getCategoryDataset() {
		CategoryDataset[] result = new CategoryDataset[getDataset().length];
		for (int i = 0; i < dataset.length; i++) {
			result[i] = (CategoryDataset) getDataset()[i];
		}
		return result;
	}

	/**
	 * @return categoryDataset
	 */
	public XYDataset[] getXYDataset() {
		XYDataset[] result = new XYDataset[getDataset().length];
		for (int i = 0; i < dataset.length; i++) {
			result[i] = (XYDataset) getDataset()[i];
		}
		return result;
	}

	/**
	 * @return pieDataset
	 */
	public PieDataset getPieDataset() {
		return (PieDataset) getDataset()[0];
	}

	/**
	 * @return color
	 */
	public Color getColor() {
		return getColor(0);
	}

	/**
	 * @return color
	 */
	public Color getColor(int index) {
		return getColors()[index];
	}

	/**
	 * @return color
	 */
	public Color[] getColors() {
		return colors;
	}

	/**
	 * @param color
	 *            セットする color
	 */
	public void setColors(Color... color) {
		this.colors = color;
	}

	/**
	 * @return bottomLabel
	 */
	public String getBottomLabel() {
		return ObjectUtils.toString(bottomLabel);
	}

	/**
	 * @param bottomLabel
	 *            セットする bottomLabel
	 */
	public void setBottomLabel(String bottomLabel) {
		this.bottomLabel = bottomLabel;
	}

	/**
	 * @return leftLabel
	 */
	public String getLeftLabel() {
		return ObjectUtils.toString(leftLabel);
	}

	/**
	 * @return rightLabel
	 */
	public String getRightLabel() {
		return ObjectUtils.toString(rightLabel);
	}

	/**
	 * @param rightLabel
	 *            セットする rightLabel
	 */
	public void setRightLabel(String rightLabel) {
		this.rightLabel = rightLabel;
	}

	/**
	 * @param leftLabel
	 *            セットする leftLabel
	 */
	public void setLeftLabel(String leftLabel) {
		this.leftLabel = leftLabel;
	}

	/**
	 * @return pieSectionIndex
	 */
	public List<Integer> getPieSectionIndex() {
		return pieSectionIndex;
	}

	/**
	 * @param pieSectionIndex
	 *            セットする pieSectionIndex
	 */
	public void setPieSectionIndex(List<Integer> pieSectionIndex) {
		this.pieSectionIndex = pieSectionIndex;
	}

	/**
	 * @return defaultLeftRange
	 */
	public boolean isDefaultLeftRange() {
		return defaultLeftRange;
	}

	/**
	 * @param defaultLeftRange
	 *            セットする defaultLeftRange
	 */
	public void setDefaultLeftRange(boolean defaultLeftRange) {
		this.defaultLeftRange = defaultLeftRange;
	}

	/**
	 * @return defaultRightRange
	 */
	public boolean isDefaultRightRange() {
		return defaultRightRange;
	}

	/**
	 * @param defaultRightRange
	 *            セットする defaultRightRange
	 */
	public void setDefaultRightRange(boolean defaultRightRange) {
		this.defaultRightRange = defaultRightRange;
	}

	/**
	 * @return leftRange
	 */
	public Range getLeftRange() {
		return leftRange;
	}

	/**
	 * @param leftRange
	 *            セットする leftRange
	 */
	public void setLeftRange(Range leftRange) {
		this.leftRange = leftRange;
	}

	/**
	 * @return title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title
	 *            セットする title
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return titleHorizontalAlignment
	 */
	public HorizontalAlignment getTitleHorizontalAlignment() {
		return titleHorizontalAlignment;
	}

	/**
	 * @param titleHorizontalAlignment
	 *            セットする titleHorizontalAlignment
	 */
	public void setTitleHorizontalAlignment(HorizontalAlignment titleHorizontalAlignment) {
		this.titleHorizontalAlignment = titleHorizontalAlignment;
	}

	/**
	 * @return chartLineLabel
	 */
	public boolean getChartLineLabel() {
		return chartLineLabel;
	}

	/**
	 * @param chartLineLabel
	 *            セットする chartLineLabel
	 */
	public void setChartLineLabel(boolean chartLineLabel) {
		this.chartLineLabel = chartLineLabel;
	}

	/**
	 * @return chartLineMarker
	 */
	public boolean getChartLineMarker() {
		return chartLineMarker;
	}

	/**
	 * @param chartLineMarker
	 *            セットする chartLineMarker
	 */
	public void setChartLineMarker(boolean chartLineMarker) {
		this.chartLineMarker = chartLineMarker;
	}

	/**
	 * データセットがカテゴリデータセットであるか確認します.
	 * 
	 * @return カテゴリデータセットの場合 true を返します.
	 */
	public boolean isCategory() {
		boolean result = true;
		for (Dataset dataset : getDataset()) {
			if (!(dataset instanceof CategoryDataset)) {
				result = false;
				break;
			}
		}
		return result;
	}
}
