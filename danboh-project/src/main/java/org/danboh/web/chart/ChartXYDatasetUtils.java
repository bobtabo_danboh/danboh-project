/*
 * Danboh is a Source Code Generation program developed by BobTabo.
 * 
 * Copyright (c) 2009 BobTabo. All Rights Reserved.
 */
package org.danboh.web.chart;

import java.awt.Color;
import java.awt.Font;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;

import org.apache.commons.lang.StringUtils;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.axis.DateTickMarkPosition;
import org.jfree.chart.axis.DateTickUnit;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.labels.StandardXYToolTipGenerator;
import org.jfree.chart.plot.DatasetRenderingOrder;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYBarRenderer;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.xy.XYDataset;

/* $Id: ChartXYDatasetUtils.java,v 1.1 2010/06/25 08:18:52 nagashiba Exp $ */

/**
 * チャート関連（XYデータセット用）のユーティリティクラスです。
 * 
 * @author <a href="mailto:nagashiba@adv-co.com">Satoshi Nagashiba</a>
 * @version $Revision: 1.1 $ $Date: 2010/06/25 08:18:52 $
 */
public final class ChartXYDatasetUtils {

	/** デフォルトフォント */
	private static final Font DEFAULT_FONT = ChartUtils.DEFAULT_FONT;

	/**
	 * コンストラクタ（外部からのインスタンス生成を許可しない）
	 */
	private ChartXYDatasetUtils() {
	}

	/**
	 * JFreeChartオブジェクトを生成します。
	 * 
	 * @param model
	 *            チャートモデル
	 * @return JFreeChartオブジェクト
	 */
	protected static JFreeChart getInstance(ChartModel model) {
		return ChartFactory.createTimeSeriesChart(StringUtils.EMPTY, model.getBottomLabel(), model.getLeftLabel(),
				model.getXYDataset()[0], true, false, true);
	}

	/**
	 * 折れ線棒グラフを設定します。
	 * 
	 * @param chart
	 *            チャート
	 * @param model
	 *            チャートモデル
	 * @return 折れ線縦棒グラフチャート
	 */
	@SuppressWarnings("deprecation")
	protected static void setXYLineBarChart(JFreeChart chart, LineBarChartModel model) {
		XYPlot plot = chart.getXYPlot();
		plot.setBackgroundPaint(Color.WHITE);
		plot.setRangeGridlinePaint(Color.BLUE);

		// 線の描画
		XYLineAndShapeRenderer lineRenderer = (XYLineAndShapeRenderer) plot.getRenderer();
		lineRenderer.setItemLabelAnchorOffset(9D);
		lineRenderer.setBaseToolTipGenerator(new StandardXYToolTipGenerator());
		lineRenderer.setSeriesPaint(LineBarChartModel.LINE, model.getColor(LineBarChartModel.LINE));
		lineRenderer.setBaseShapesVisible(true);
		lineRenderer.setDrawOutlines(true);
		lineRenderer.setSeriesShape(0, new java.awt.geom.Ellipse2D.Double(-2D, -2D, 4D, 4D));

		// 棒グラフ設定
		plot.setDataset(LineBarChartModel.BAR, model.getXYDataset()[LineBarChartModel.BAR]);
		plot.mapDatasetToRangeAxis(LineBarChartModel.LINE, LineBarChartModel.LINE);
		plot.mapDatasetToRangeAxis(LineBarChartModel.BAR, LineBarChartModel.BAR);
		plot.setDatasetRenderingOrder(DatasetRenderingOrder.REVERSE);

		// 棒の描画
		XYBarRenderer barRenderer = new XYBarRenderer();
		barRenderer.setSeriesPaint(LineBarChartModel.BAR, model.getColor(LineBarChartModel.BAR));
		barRenderer.setShadowVisible(false);
		plot.setRenderer(LineBarChartModel.BAR, barRenderer);

		// 横軸の設定
		DateAxis domainAxis = (DateAxis) plot.getDomainAxis();
		domainAxis.setTickLabelsVisible(true);
		domainAxis.setTickLabelFont(DEFAULT_FONT);
		domainAxis.setLabelFont(DEFAULT_FONT);
		domainAxis.setVerticalTickLabels(true);
		domainAxis.setTickMarkPosition(DateTickMarkPosition.MIDDLE);
		domainAxis.setTickUnit(new DateTickUnit(DateTickUnit.DAY, model.getDateAxisRange(), new SimpleDateFormat(
				"yyyy/MM/dd")));

		// 縦軸（左）の設定
		NumberAxis leftRangeAxis = (NumberAxis) plot.getRangeAxis();
		leftRangeAxis.setTickLabelFont(DEFAULT_FONT);
		leftRangeAxis.setLabelFont(DEFAULT_FONT);
		leftRangeAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());
		setAxisRangeColumn(leftRangeAxis, model.getXYDataset()[LineBarChartModel.LINE]);

		// 縦軸（右）の設定
		NumberAxis raightRangeAxis = new NumberAxis(model.getRightLabel());
		setAxisRangeColumn(raightRangeAxis, model.getXYDataset()[LineBarChartModel.BAR]);
		raightRangeAxis.setTickLabelFont(DEFAULT_FONT);
		raightRangeAxis.setLabelFont(DEFAULT_FONT);
		raightRangeAxis.setNumberFormatOverride(NumberFormat.getIntegerInstance());
		raightRangeAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());
		plot.setRangeAxis(LineBarChartModel.BAR, raightRangeAxis);

		// 棒の色
		XYItemRenderer renderer = null;
		renderer = plot.getRenderer(LineBarChartModel.BAR);
		renderer.setSeriesPaint(0, model.getColor(LineBarChartModel.BAR));
	}

	/**
	 * 縦軸ラベルを設定します
	 * 
	 * @param axis
	 * @param dataset
	 */
	private static void setAxisRangeColumn(NumberAxis axis, XYDataset dataset) {
		double value = 0D;
		for (int i = 0; i < dataset.getSeriesCount(); i++) {
			value += dataset.getX(0, i).doubleValue();
		}
		if (value == 0D) {
			axis.setRange(ChartUtils.DEFAULT_RANGE);
		} else {
			axis.setAutoRange(true);
		}
	}
}
