package org.danboh.web.action;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Actions;
import org.apache.struts2.convention.annotation.Result;

import org.danboh.web.annotation.BeforeLogin;
import org.danboh.web.annotation.RequiredSSL;
import org.danboh.web.util.ActionUtil;
import com.opensymphony.xwork2.ActionSupport;

/**
 * エラーページ振り分け用Action
 */
@RequiredSSL
@BeforeLogin
@Result(name = "success", type = "mayaa", location = "/${applicationPath}error/${errorCode}.html")
public class ErrorAction extends ActionSupport {
	private static final long serialVersionUID = -5148611191743082822L;

	public HttpServletRequest request;
	public String applicationPath = "";
	public String errorCode = "";

	@Override
	@Actions( { @Action(value = "/403"), @Action(value = "/404"), @Action(value = "/500") })
	public String execute() throws Exception {
		errorCode = ActionUtil.getActionName();
		// String contextPath = "";
		// if (StringUtils.isNotBlank(request.getContextPath())) {
		// contextPath = request.getContextPath();
		// }

		applicationPath = "admin/";

		return SUCCESS;
	}
}
