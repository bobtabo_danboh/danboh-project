package org.danboh.web.action;

import java.io.ByteArrayInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;


import org.danboh.web.ext.struts2.interceptor.annotations.AllowFields;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

@AllowFields(fields = { "value" })
public class ImageViewAction extends ActionSupport {
	private static final long serialVersionUID = -1704291758727196788L;

	/** ファイル内容のプロパティ */
	private String value;

	/** Content-Type */
	private String contentType;

	/** ダウンロード用InputStream */
	private InputStream inputStream;

	@Override
	public String execute() throws FileNotFoundException {
		byte[] fileValue = (byte[]) ActionContext.getContext().getValueStack().findValue(value);
		if (fileValue == null) {
			throw new FileNotFoundException();
		}

		contentType = (String) ActionContext.getContext().getValueStack().findValue(value + "FileContentType");
		inputStream = new ByteArrayInputStream(fileValue);

		return SUCCESS;
	}

	public String getContentType() {
		return contentType;
	}

	public InputStream getInputStream() {
		return inputStream;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String v) {
		this.value = v;
	}
}
