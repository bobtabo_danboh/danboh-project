package org.danboh.web.log4j;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;

/**
 * 日次ローテートAppenderに世代管理処理を追記したAppender。
 * 
 * @author osaki
 */
public class DailyRollingFileAppender extends org.apache.log4j.DailyRollingFileAppender {

	/**
	 * 最大バックアップ世代数
	 */
	protected int maxBackupIndex = 0;

	/**
	 * ファイル作成時に世代数をチェック
	 */
	@Override
	public void setFile(String arg0, boolean arg1, boolean arg2, int arg3) throws IOException {
		super.setFile(arg0, arg1, arg2, arg3);

		// ログファイル数をチェック
		checkFileCount();
	}

	/**
	 * 世代管理を実行するメソッド
	 */
	private void checkFileCount() {

		// ログファイルインスタンスを取得
		File[] logFiles = getLogFiles();

		// バックアップ数のチェック
		int diff = logFiles.length - maxBackupIndex;
		if (fileName == null || diff <= 0) {
			return;
		}
		// 更新日が古い順にソート
		Arrays.sort(logFiles, new FilenameComparator());
		// 削除実行
		for (int i = 0; i < diff; i++) {
			logFiles[i].delete();
		}
	}

	/**
	 * 全ログファイルを取得する
	 * 
	 * @return 全ログファイル
	 */
	private File[] getLogFiles() {
		File file = new File(fileName);
		File logDir = file.getParentFile();
		if (logDir == null) {
			logDir = new File(".");
		}
		return logDir.listFiles(new FilenameFilter(file.getName()));
	}

	/**
	 * 最大バックアップ世代数を設定する
	 * 
	 * @param maxBackupIndex
	 *            最大バックアップ世代数
	 */
	public void setMaxBackupIndex(int maxBackupIndex) {
		if (maxBackupIndex > 0) {
			this.maxBackupIndex = maxBackupIndex;
		}
	}
}
