package org.danboh.web.log4j;

import java.io.File;

/**
 * ファイル名のプリフィクスからログファイルかどうか判断するフィルタ。
 * 
 * @author osaki
 */
public class FilenameFilter implements java.io.FilenameFilter {

	/**
	 * ログファイルのベース名
	 */
	private String baseName;

	/**
	 * コンストラクタ
	 * 
	 * @param baseName
	 *            ログファイルのベース名
	 */
	public FilenameFilter(String baseName) {
		this.baseName = baseName;
	}

	/**
	 * nameがbaseNameで始まればtrue、始まらなければfalseを返す。
	 * 
	 * @param dir
	 *            検証対象ファイルディレクトリ
	 * @param name
	 *            検証対象ファイル名
	 */
	@Override
	public boolean accept(File dir, String name) {
		if (baseName == null || name == null) {
			return false;
		}
		return name.startsWith(baseName);
	}
}
