package org.danboh.web.log4j;

import java.io.File;
import java.util.Comparator;

/**
 * ログファイルソート用のコンパレータ。
 * 
 * @author osaki
 */
public class FilenameComparator implements Comparator<File> {

	/**
	 * 最終更新日付を比較し、古い順にソートする
	 * 
	 * @param o1
	 *            比較元1
	 * @param o2
	 *            比較元2
	 * @return 比較結果
	 */
	@Override
	public int compare(File o1, File o2) {

		// いずれかのインスタンスがnullの場合は0
		if (o1 == null || o2 == null) {
			return 0;
		}

		// 桁落ち回避のためキャストはしない
		long result = o1.lastModified() - o2.lastModified();
		if (result > 0) {
			return 1;
		}
		if (result < 0) {
			return -1;
		}
		return 0;
	}
}
