package org.danboh.web.dto;

import java.io.Serializable;

import org.danboh.web.util.ConstSettingUtil;

public class PagerCondition implements Serializable {
	private static final long serialVersionUID = 3696499750012678731L;
	private static final int DEFAULT_LIMIT = ConstSettingUtil.getInteger("pager.defaultLimit");
	private int offset = 0;
	private int limit = DEFAULT_LIMIT;
	private int count;
	private String orderBy = "";

	public int getOffset() {
		return offset;
	}

	public void setOffset(int offset) {
		this.offset = offset;
	}

	public int getLimit() {
		return limit;
	}

	public void setLimit(int limit) {
		this.limit = limit;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public String getOrderBy() {
		return orderBy;
	}

	public void setOrderBy(String orderBy) {
		this.orderBy = orderBy;
	}

	public boolean hasNextPage() {
		return (offset + limit) < count;
	}

	public boolean hasPrevPage() {
		return offset != 0;
	}

	public int getPageNumber() {
		return offset / ((limit == 0) ? DEFAULT_LIMIT : limit) + 1;
	}

	public int getPageCount() {
		return (count - 1) / ((limit == 0) ? DEFAULT_LIMIT : limit) + 1;
	}
}
