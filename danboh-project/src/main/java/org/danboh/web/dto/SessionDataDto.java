package org.danboh.web.dto;

import org.seasar.framework.container.annotation.tiger.Component;
import org.seasar.framework.container.annotation.tiger.InstanceType;

@Component(instance = InstanceType.SESSION)
public class SessionDataDto {
	// /** アカウントタイプ */
	// public AccountType accountType;
	//
	// /** 管理者アカウント */
	// public Manager manager;
	//
	// /** 代理店 */
	// public Client client;
	//
	// /** 会員 */
	// public Member member;

	/** 再認証している */
	public boolean isReLogin = false;

	/** ログイン後に遷移するパス */
	public String afterLoginPath;

	public boolean isLogin() {
		// if (accountType == null) {
		// return false;
		// }
		//
		// switch (accountType) {
		// case ADMIN:
		// return manager != null;
		// case CLIENT:
		// return client != null;
		// case USER:
		// return member != null;
		// default:
		// break;
		// }

		return false;
	}
}
