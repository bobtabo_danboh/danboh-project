package org.danboh.web.exceptions;

public class ForeignKeyViolationException extends RuntimeException {
	private static final long serialVersionUID = 682839077173839556L;

	public ForeignKeyViolationException() {
		super();
	}

	public ForeignKeyViolationException(String message, Throwable cause) {
		super(message, cause);
	}

	public ForeignKeyViolationException(String message) {
		super(message);
	}

	public ForeignKeyViolationException(Throwable cause) {
		super(cause);
	}
}
