/*
 * Danboh is a Source Code Generation program developed by BobTabo.
 * 
 * Copyright (c) 2009 BobTabo. All Rights Reserved.
 */
package org.danboh.web.exceptions;

/* $Id: AppException.java,v 1.1 2010/06/11 04:03:40 nagashiba Exp $ */

/**
 * システム例外クラスです。
 * 
 * @author <a href="mailto:nagashiba@adv-co.com">Satoshi Nagashiba</a>
 * @version $Revision: 1.1 $ $Date: 2010/06/11 04:03:40 $
 */
public class AppException extends RuntimeException {

	/** 　シリアルバージョンID　 */
	private static final long serialVersionUID = 8080156003121348364L;

	/**
	 * コンストラクタ
	 */
	public AppException() {
		super();
	}

	/**
	 * コンストラクタ
	 * 
	 * @param s
	 */
	public AppException(String s) {
		super(s);
	}

	/**
	 * コンストラクタ
	 * 
	 * @param s
	 * @param throwable
	 */
	public AppException(String s, Throwable throwable) {
		super(s, throwable);
	}

	/**
	 * コンストラクタ
	 * 
	 * @param throwable
	 */
	public AppException(Throwable throwable) {
		super(throwable);
	}
}
