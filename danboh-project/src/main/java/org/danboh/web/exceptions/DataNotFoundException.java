package org.danboh.web.exceptions;

public class DataNotFoundException extends RuntimeException {
	private static final long serialVersionUID = 1092112078446489052L;

	public DataNotFoundException() {
		super();
	}

	public DataNotFoundException(String message, Throwable cause) {
		super(message, cause);
	}

	public DataNotFoundException(String message) {
		super(message);
	}

	public DataNotFoundException(Throwable cause) {
		super(cause);
	}
}
