package org.danboh.web.exceptions;

public class ForbiddenException extends RuntimeException {
	private static final long serialVersionUID = -2781612929442852926L;

	public ForbiddenException() {
		super();
	}

	public ForbiddenException(String message, Throwable cause) {
		super(message, cause);
	}

	public ForbiddenException(String message) {
		super(message);
	}
}
