package org.danboh.web.exceptions;

public class PageNotFoundException extends RuntimeException {
	private static final long serialVersionUID = -303988992787937897L;

	public PageNotFoundException() {
		super();
	}

	public PageNotFoundException(String message, Throwable cause) {
		super(message, cause);
	}

	public PageNotFoundException(String message) {
		super(message);
	}

	public PageNotFoundException(Throwable cause) {
		super(cause);
	}
}
