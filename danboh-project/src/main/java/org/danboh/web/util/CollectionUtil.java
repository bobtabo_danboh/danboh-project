package org.danboh.web.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.apache.commons.collections.CollectionUtils;

/* $Id: CollectionUtil.java,v 1.2 2010/07/19 08:45:44 nagashiba Exp $ */

/**
 * コレクション関連のユーティリティクラスです。
 * 
 * @author <a href="mailto:nagashiba@adv-co.com">Satoshi Nagashiba</a>
 * @version $Revision: 1.2 $ $Date: 2010/07/19 08:45:44 $
 */
public class CollectionUtil {

	/**
	 * 対象リストからランダムな値を取得します。
	 * 
	 * @param list
	 *            対象リスト
	 * @return ランダムな値
	 */
	public static <E> E randomOfList(List<E> list) {
		if (list.size() == 0) {
			return null;
		} else if (list.size() == 1) {
			return list.get(0);
		} else {
			return list.get(random(list.size() - 1));
		}
	}

	/**
	 * 対象リストの順序をランダムに並び替えたリストを作成します。
	 * 
	 * @param list
	 *            対象リスト
	 * @return ランダムリスト
	 */
	@SuppressWarnings("unchecked")
	public static List randomList(List list) {
		List tmpList = new ArrayList();

		while (list.size() > 0) {
			int choice = random(list.size() - 1);
			tmpList.add(list.get(choice));
			list.remove(choice);
		}

		return tmpList;
	}

	/**
	 * 指定された最大値以内のランダム数値を取得します。
	 * 
	 * @param max
	 *            ランダム数値の最大値
	 * @return ランダム数値
	 */
	public static int random(int max) {
		return (int) Math.abs(new Random().nextInt() % (max + 1));
	}

	public static <E> List<List<E>> recombinantFillList(List<E> list, int unitSize) {
		fillList(list, unitSize, null);
		return recombinantList(list, unitSize);
	}

	public static <E> List<List<E>> recombinantList(List<E> list, int unitSize) {
		if (unitSize < 1) {
			return null;
		}
		if (list != null && list.size() > 0) {
			List<List<E>> resultList = new ArrayList<List<E>>();
			List<E> unitList = new ArrayList<E>(unitSize);
			for (E e : list) {
				if (unitList.size() == unitSize) {
					resultList.add(unitList);
					unitList = new ArrayList<E>(unitSize);
					unitList.add(e);
				} else {
					unitList.add(e);
				}
			}
			if (unitList != null && unitList.size() > 0) {
				fillList(unitList, unitSize, null);
				resultList.add(unitList);
			}
			return resultList;
		}
		return null;
	}

	public static <E> List<E> fillList(List<E> list, int unitSize, E defaul) {
		if (list == null) {
			list = new ArrayList<E>();
		}
		while (list.size() < unitSize) {
			list.add(defaul);
		}
		return list;
	}

	/**
	 * 最後のインデックスを取得します。
	 * 
	 * @param list
	 *            リスト
	 * @return 最後のインデックス
	 */
	public static <E> int getLastIndex(List<E> list) {
		int result = -1;
		if (CollectionUtils.isEmpty(list)) {
			return result;
		}
		return list.size() - 1;
	}
}
