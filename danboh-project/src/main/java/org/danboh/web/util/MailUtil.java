package org.danboh.web.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

/* $Id: MailUtil.java,v 1.3 2010/12/17 05:02:55 nagashiba Exp $ */

/**
 * メール送信用のユーティリティクラスです。
 *
 * @author <a href="mailto:nagashiba@prov-co.com">Satoshi Nagashiba</a>
 * @version $Revision: 1.3 $ $Date: 2010/12/17 05:02:55 $
 */
public class MailUtil {

	/** 管理者のメールアドレス */
	@SuppressWarnings("unused")
	private static final String ADMIN_MAILADDRESS = EnvSettingUtil.getString("admin.mailAddress");

	/** フロントドメイン */
	private static final String FRONT_DOMAIN = EnvSettingUtil.getString("frontDomain");

	/**
	 * メールアドレスを解析します。
	 *
	 * @param mailAddress
	 *            メールアドレス
	 * @return メールアドレスとして認識されない場合 false を返します
	 */
	public static boolean parse(String mailAddress) {
		try {
			InternetAddress.parse(mailAddress);
			Pattern ptn = Pattern.compile("[\\w\\.\\-]+@(?:[\\w\\-]+\\.)+[\\w\\-]+");
			Matcher mc = ptn.matcher(mailAddress);
			if (mc.matches()) {
				return true;
			} else {
				return false;
			}
		} catch (AddressException e) {
			return false;
		}
	}
	//
	// /**
	// * 入会メールを送信します。
	// *
	// * @param member
	// * 会員エンティティ
	// */
	// public static void sendMemberRegistCompleteMail(Member member) {
	// Map<String, Object> contentMap = new HashMap<String, Object>();
	// contentMap.put("nickName", member.getName());
	// contentMap.put("domain", FRONT_DOMAIN);
	// sendMail(contentMap, "member-regist-complete.xml",
	// member.getMailAddress());
	// }
	//
	// /**
	// * ID・パスワード再発行メールを送信します。
	// *
	// * @param member
	// * 会員エンティティ
	// */
	// public static void sendPasswordReminderMail(Member member) {
	// Map<String, Object> contentMap = new HashMap<String, Object>();
	// contentMap.put("nickName", member.getName());
	// contentMap.put("loginId", member.getMailAddress());
	// contentMap.put("password", member.getPassword());
	// contentMap.put("domain", FRONT_DOMAIN);
	// sendMail(contentMap, "password-reminder.xml", member.getMailAddress());
	// }
	//
	// /**
	// * 退会メールを送信します。
	// *
	// * @param member
	// * 会員エンティティ
	// */
	// public static void sendMemberResignCompleteMail(Member member) {
	// Map<String, Object> contentMap = new HashMap<String, Object>();
	// contentMap.put("nickName", member.getName());
	// contentMap.put("domain", FRONT_DOMAIN);
	// sendMail(contentMap, "member-resign-complete.xml",
	// member.getMailAddress());
	// }
	//
	// /**
	// * メールを送信します。
	// *
	// * @param dto
	// * バインドするオブジェクト
	// * @param template
	// * メールテンプレート
	// * @param to
	// * 宛先メールアドレス
	// */
	// private static void sendMail(Object dto, String template, String to) {
	// MailProcessor.sendMail(dto, "/mails/template/" + template, to);
	// }
}
