package org.danboh.web.util;

/* $Id: EnumUtil.java,v 1.1 2010/06/11 04:03:41 nagashiba Exp $ */


import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.beanutils.MethodUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;

import org.danboh.web.exceptions.AppException;

/**
 * Enumのユーティリティクラスです。
 * 
 * @author <a href="mailto:nagashiba@adv-co.com">Satoshi Nagashiba</a>
 * @version $Revision: 1.1 $ $Date: 2010/06/11 04:03:41 $
 */
public class EnumUtil {

	/**
	 * Enumリストをカンマ区切り文字列に変換します。
	 * 
	 * @param contents
	 *            Enumリスト
	 * @return カンマ区切り文字列
	 */
	public static <E> String toComma(List<E> contents) {
		if (CollectionUtils.isEmpty(contents)) {
			return null;
		}
		StringBuilder result = new StringBuilder();
		for (E content : contents) {
			if (StringUtils.isNotEmpty(result.toString())) {
				result.append(",");
			}
			if (content == null) {
				continue;
			}
			Object value = BeanUtil.getProperty(content, "value");
			result.append(String.valueOf(value));
		}
		return result.toString();
	}

	/**
	 * カンマ区切り文字列をEnumリストに変換します。
	 * 
	 * @param enumClass
	 *            Enumクラス
	 * @param comma
	 *            カンマ区切り文字列
	 * @return Enumリスト
	 */
	@SuppressWarnings("unchecked")
	public static <E> List<E> toList(Class<E> enumClass, String comma) {
		List<E> result = new ArrayList<E>();

		if (StringUtils.isEmpty(comma)) {
			return result;
		}
		String[] types = StringUtils.split(comma, ",");
		for (String type : types) {
			E object = null;
			try {
				object = (E) MethodUtils.invokeStaticMethod(enumClass, "valueOf", Integer.parseInt(type));
			} catch (NumberFormatException e) {
				throw new AppException(e);
			} catch (NoSuchMethodException e) {
				throw new AppException(e);
			} catch (IllegalAccessException e) {
				throw new AppException(e);
			} catch (InvocationTargetException e) {
				throw new AppException(e);
			}
			result.add(object);
		}

		return result;
	}
}
