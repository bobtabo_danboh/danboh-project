package org.danboh.web.util;

import org.apache.commons.lang.ObjectUtils;
import org.apache.commons.lang.StringUtils;

/* $Id: StringUtil.java,v 1.4 2010/07/23 07:50:05 minagawa Exp $ */

/**
 * 文字列に対する共通処理を行います。
 * 
 * @author <a href="mailto:nagashiba@adv-co.com">Satoshi Nagashiba</a>
 * @version $Revision: 1.4 $ $Date: 2010/07/23 07:50:05 $
 */
public final class StringUtil {

	/**
	 * 文字列を返します。
	 * 
	 * @param str
	 *            対象文字列
	 * @param nullStr
	 *            文字列が空orNull時に置き換える文字列
	 * @return 文字列
	 */
	public static String toString(String str, String nullStr) {
		return StringUtils.isEmpty(str) ? nullStr : str;
	}

	/**
	 * オブジェクトが Null であるか確認します。
	 * 
	 * @param obj
	 *            オブジェクト
	 * @return Null の場合 true を返します
	 */
	public static boolean isEmpty(Object obj) {
		return StringUtils.isEmpty(ObjectUtils.toString(obj));
	}

	/**
	 * オブジェクトが Null ではないか確認します。
	 * 
	 * @param obj
	 *            オブジェクト
	 * @return Null ではない場合 true を返します
	 */
	public static boolean isNotEmpty(Object obj) {
		return (!isEmpty(obj));
	}

	/**
	 * データが0またはnullであった場合、"-"表記に変更します。
	 * 
	 * @param Integer
	 *            rank
	 * @return 0ないしはNullの場合"-"を返します。
	 */
	public static String checkZeroData(Integer rank) {
		return NumberUtil.isZero(rank) ? "-" : rank.toString();
	}

	/**
	 * データが0またはnullであった場合、"-"表記に変更します。
	 * 
	 * @param Long
	 *            rank
	 * @return 0ないしはNullの場合"-"を返します。
	 */
	public static String checkZeroData(Long rank) {
		return NumberUtil.isZero(rank) ? "-" : rank.toString();
	}

	/**
	 * アンダースコア（_）記法の文字列をキャメル記法に変換します。
	 * 
	 * @param str
	 *            文字列
	 * @return 変換した文字列
	 */
	public static String camelize(String str) {
		String[] strings = StringUtils.split(str.toLowerCase(), "_");
		for (int i = 1; i < strings.length; i++) {
			strings[i] = StringUtils.capitalize(strings[i]);
		}
		return StringUtils.join(strings);
	}

	/**
	 * 文字列の先頭に http:// もしくは https://　が付加されてるか確認します。
	 * 
	 * @param str
	 *            文字列
	 * @return 付加されている場合 true を返します
	 */
	public static boolean isHttp(String str) {
		String http = StringUtils.substring(str, 0, "http://".length());
		String https = StringUtils.substring(str, 0, "https://".length());

		return (StringUtils.equals(http, "http://") || StringUtils.equals(https, "https://"));
	}

	/**
	 * 文字列の先頭から http:// を除いた文字列を取得します。
	 * 
	 * @param str
	 *            文字列
	 * @return http:// を除いた文字列
	 */
	public static String removeHttp(String str) {
		return StringUtils.replaceOnce(str, "http://", StringUtils.EMPTY);
	}

	/**
	 * 文字列の先頭に http:// を付加した文字列を取得します。
	 * 
	 * @param str
	 *            文字列
	 * @return http:// を付加した文字列
	 */
	public static String addHttp(String str) {
		return (!isHttp(str)) ? "http://" + str : str;
	}
}
