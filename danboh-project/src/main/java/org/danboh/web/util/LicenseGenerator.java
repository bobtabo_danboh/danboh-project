/*
 * Danboh is a Source Code Generation program developed by BobTabo.
 * 
 * Copyright (c) 2009 BobTabo. All Rights Reserved.
 */
package org.danboh.web.util;

import java.security.SecureRandom;

public class LicenseGenerator {

	/** デフォルトの桁数 */
	private static final int DEFALUT_LENGTH = 30;

	/**
	 * 「0-9」「A-Z」「a-z」を使用したパスワードを生成します。
	 * 
	 * @param seed
	 * @param length
	 * @return
	 */
	public static String generatePassword(byte[] seed, int length) {
		SecureRandom rand = null;
		char[] password = new char[length];
		int number = 0;

		if (seed == null) {
			rand = new SecureRandom();
		} else {
			rand = new SecureRandom(seed);
		}

		for (int i = 0; i < length; i++) {
			number = rand.nextInt(62);
			if (number < 10) {
				// 数字
				password[i] = (char) (48 + number);
			} else if (number < 36) {
				// アルファベット大文字
				password[i] = (char) (55 + number);
			} else {
				// アルファベット小文字
				password[i] = (char) (61 + number);
			}
		}

		return new String(password);
	}

	/**
	 * 「0-9」「A-Z」「a-z」を使用したパスワードを生成します。
	 * 
	 * @return
	 */
	public static String generatePassword() {
		return generatePassword((byte[]) null, DEFALUT_LENGTH);
	}

	/**
	 * 「0-9」「A-Z」「a-z」を使用したパスワードを生成します。
	 * 
	 * @param seed
	 * @return
	 */
	public static String generatePassword(byte[] seed) {
		return generatePassword(seed, DEFALUT_LENGTH);
	}

	/**
	 * 「0-9」「A-Z」「a-z」を使用したパスワードを生成します。
	 * 
	 * @param length
	 * @return
	 */
	public static String generatePassword(int length) {
		return generatePassword((byte[]) null, length);
	}

	/**
	 * 「0-9」「A-Z」「a-z」を使用したパスワードを生成します。
	 * 
	 * @param seed
	 *            シングルバイト文字のみ
	 * @return
	 */
	public static String generatePassword(String seed) {
		return generatePassword(seed.getBytes());
	}

	/**
	 * 「0-9」「A-Z」「a-z」を使用したパスワードを生成します。
	 * 
	 * @param seed
	 *            シングルバイト文字のみ
	 * @param length
	 * @return
	 */
	public static String generatePassword(String seed, int length) {
		return generatePassword(seed.getBytes(), length);
	}
}
