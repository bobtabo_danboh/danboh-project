package org.danboh.web.util;

import java.util.ResourceBundle;

public class EnvSettingUtil {
	public static ResourceBundle setting = ResourceBundle.getBundle("envSetting");

	public static String getString(String key) {
		return setting.getString(key);
	}

	public static String[] getStringArray(String key) {
		return setting.getStringArray(key);
	}

	public static Integer getInteger(String key) {
		return Integer.parseInt(getString(key));
	}

	public static boolean isTrue(String key) {
		return "true".equals(setting.getString(key));
	}
}
