package org.danboh.web.util;

import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.commons.beanutils.PropertyUtils;

public class ImageProducer {
	/** 画像保存パス */
	public String imageSavePath;
	/** 画像保存パス(携帯) */
	public String mobileImageSavePath;

	/** 表示用画像パス */
	public String imagePath;
	/** 表示用画像パス(モバイル) */
	public String mobileImagePath;

	public static Map<String, String> contentTypeExtentionMap = new LinkedHashMap<String, String>();
	static {
		contentTypeExtentionMap.put("image/jpeg", ".jpg");
		contentTypeExtentionMap.put("image/pjpeg", ".jpg");
		contentTypeExtentionMap.put("image/gif", ".gif");
		contentTypeExtentionMap.put("image/png", ".png");
		contentTypeExtentionMap.put("application/x-shockwave-flash", ".swf");
	}
	public static Map<String, String> mobileContentTypeExtentionMap = new LinkedHashMap<String, String>();
	static {
		mobileContentTypeExtentionMap.put("image/jpeg", ".jpg");
		mobileContentTypeExtentionMap.put("image/pjpeg", ".jpg");
		mobileContentTypeExtentionMap.put("image/gif", ".gif");
		mobileContentTypeExtentionMap.put("image/png", ".gif");
	}

	public static Map<Class<?>, String[]> imageDirectoryMap = new LinkedHashMap<Class<?>, String[]>();
	// static {
	// imageDirectoryMap.put(ClientImage.class, new String[] { "client",
	// "clientId" });
	// }

	/**
	 * 画像の保存用パスを取得する。
	 *
	 * @param entity
	 *            エンティティ
	 * @return
	 */
	public String[] getImageSavePath(Object entity) {
		String[] directoryInfo = ImageProducer.imageDirectoryMap.get(entity.getClass());
		if (directoryInfo == null) {
			return null;
		}
		String subdirectory = null;
		try {
			int id = (Integer) PropertyUtils.getProperty(entity, directoryInfo[1]);
			subdirectory = (id % 1000) + File.separator + id;
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		}
		String pcDirectry = imageSavePath;
		long updateDate = 0L;
		try {
			updateDate = ((Date) PropertyUtils.getProperty(entity, "updateDate")).getTime();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		}
		pcDirectry += File.separator + directoryInfo[0] + File.separator + subdirectory + File.separator + updateDate
				+ File.separator;

		String mobileDirectory = mobileImageSavePath;
		mobileDirectory += File.separator + directoryInfo[0] + File.separator + subdirectory + File.separator
				+ updateDate + File.separator;

		return new String[] { pcDirectry, mobileDirectory };
	}

	/**
	 * 参照用の画像パスを取得する。
	 *
	 * @param entity
	 *            エンティティクラス
	 * @param identity
	 *            キー
	 * @param updateDate
	 *            更新時間
	 * @param mobile
	 *            携帯用
	 *
	 * @return
	 */
	public String getImagePath(Class<?> entity, String identity, long updateDate, boolean mobile) {
		String[] directoryInfo = ImageProducer.imageDirectoryMap.get(entity);
		int id = Integer.parseInt(identity);
		String subdirectory = (id % 1000) + "/" + id;

		String result;
		if (mobile) {
			result = mobileImagePath;
		} else {
			result = imagePath;
		}

		result += "/" + directoryInfo[0] + "/" + subdirectory + "/" + updateDate + "/";

		return result;
	}
}
