package org.danboh.web.util;

import java.util.Collection;
import java.util.List;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

/* $Id: ActionUtil.java,v 1.1 2010/06/11 04:03:41 nagashiba Exp $ */

/**
 * アクション関連のユーティリティクラスです。
 * 
 * @author <a href="mailto:kosugi@prov-co.com">kosugi</a>
 * @version $Revision: 1.1 $ $Date: 2010/06/11 04:03:41 $
 */
public class ActionUtil {

	/**
	 * 実行されているアクションの名前を取得します。
	 * 
	 * @return 実行されているアクションの名前
	 */
	public static String getActionName() {
		return ActionContext.getContext().getName();
	}

	/**
	 * 実行されているアクションのメソッド名を取得します。
	 * 
	 * @return 実行されているアクションのメソッド名
	 */
	public static String getMethodName() {
		return ActionContext.getContext().getActionInvocation().getProxy().getMethod();
	}

	/**
	 * 実行されているアクションを取得します。
	 * 
	 * @return 実行されているアクション
	 */
	public static ActionSupport getActionSupport() {
		Object action = ActionContext.getContext().getActionInvocation().getAction();
		if (action instanceof ActionSupport) {
			return (ActionSupport) action;
		}
		return null;
	}

	/**
	 * 実行されているアクションの ActionErrors を取得します。
	 * 
	 * @return 実行されているアクションの ActionErrors
	 */
	public static Collection<String> getActionErrors() {
		ActionSupport action = getActionSupport();
		if (action != null) {
			return action.getActionErrors();
		}
		return null;
	}

	/**
	 * 実行されているアクションの ActionMessages を取得します。
	 * 
	 * @return 実行されているアクションの ActionMessages
	 */
	public static Collection<String> getActionMessages() {
		ActionSupport action = getActionSupport();
		if (action != null) {
			return action.getActionMessages();
		}
		return null;
	}

	/**
	 * 実行されているアクションの FiledErrors を取得します。
	 * 
	 * @param fieldName
	 *            フィールド名
	 * @return 実行されているアクションの FiledErrors
	 */
	public static List<String> getFieldErrors(String fieldName) {
		ActionSupport action = getActionSupport();
		if (action != null) {
			return action.getFieldErrors().get(fieldName);
		}
		return null;
	}

	/**
	 * ネームスペースを取得します。
	 * 
	 * @return ネームスペース
	 */
	public static String getNameSpace() {
		return ActionContext.getContext().getActionInvocation().getProxy().getNamespace();
	}
}
