package org.danboh.web.util;


import java.io.UnsupportedEncodingException;

import javax.mail.internet.InternetAddress;

import org.apache.commons.lang.StringUtils;
import org.apache.velocity.VelocityContext;

import org.danboh.web.exceptions.AppException;
import com.ozacc.mail.Mail;
import com.ozacc.mail.MailException;
import com.ozacc.mail.SendMail;
import com.ozacc.mail.VelocityMailBuilder;
import com.ozacc.mail.impl.SendMailImpl;
import com.ozacc.mail.impl.XMLVelocityMailBuilderImpl;

/**
 * メールを送信するクラスです。
 * 
 * @author <a href="mailto:nagashiba@prov-co.com">Satoshi Nagashiba</a>
 * @version $Revision: 1.1 $ $Date: 2010/06/11 04:03:41 $
 */
public class MailProcessor {

	private static final String MAIL_SMTP_SERVER = EnvSettingUtil.getString("mail.smtp.server");
	private static final String MAIL_ENCODING_CHARSET = EnvSettingUtil.getString("mail.encoding.charset");
	private static final String SYSTEM_MAILADDRESS = EnvSettingUtil.getString("system.mailAddress");
	private static final String SYSTEM_DISPLAYNAME = EnvSettingUtil.getString("system.displayName");

	/**
	 * メールを送信します。
	 * 
	 * @param template
	 *            メールテンプレート情報
	 * @throws MailException
	 *             メール送信例外
	 */
	public static void sendMail(Object dto, String template, String to) throws MailException {
		SendMail sendMail = new SendMailImpl(MAIL_SMTP_SERVER);
		((SendMailImpl) sendMail).setCharset(MAIL_ENCODING_CHARSET);

		// OZACCメールオブジェクトを生成します
		Mail mail = build(dto, template);

		if (StringUtils.isNotEmpty(to)) {
			mail.addTo(to);
		}
		mail.setFrom(getFromMailAddress());

		sendMail.send(mail);
	}

	/**
	 * メールを生成します
	 * 
	 * @param dto
	 *            DTO
	 * @param template
	 *            メールテンプレート
	 * @return OZACCメールオブジェクト
	 */
	private static Mail build(Object dto, String template) {
		VelocityMailBuilder builder = new XMLVelocityMailBuilderImpl();
		VelocityContext context = new VelocityContext();
		context.put("dto", dto);
		return builder.buildMail(template, context);
	}

	/**
	 * 送信元メールアドレスを取得します。
	 * 
	 * @return 送信元メールアドレス
	 */
	public static InternetAddress getFromMailAddress() {
		try {
			return new InternetAddress(SYSTEM_MAILADDRESS, SYSTEM_DISPLAYNAME, "ISO-2022-JP");
		} catch (UnsupportedEncodingException e) {
			throw new AppException(e);
		}
	}
}
