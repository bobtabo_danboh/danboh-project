package org.danboh.web.util;

import java.util.ResourceBundle;

/* $Id: ConstSettingUtil.java,v 1.1 2010/06/11 04:03:41 nagashiba Exp $ */

/**
 * 「constSetting.properties」の設定値を取得する為のユーティリティクラスです。
 * 
 * @author <a href="mailto:kosugi@prov-co.com">kosugi</a>
 * @version $Revision: 1.1 $ $Date: 2010/06/11 04:03:41 $
 */
public class ConstSettingUtil {
	public static ResourceBundle setting = ResourceBundle.getBundle("constSetting");

	/**
	 * 文字列の設定値を取得します。
	 * 
	 * @param key
	 *            キー
	 * @return 文字列の設定値
	 */
	public static String getString(String key) {
		return setting.getString(key);
	}

	/**
	 * 文字列配列の設定値を取得します。
	 * 
	 * @param key
	 *            キー
	 * @return 文字列配列の設定値
	 */
	public static String[] getStringArray(String key) {
		return setting.getStringArray(key);
	}

	/**
	 * 数値の設定値を取得します。
	 * 
	 * @param key
	 *            キー
	 * @return 数値の設定値
	 */
	public static Integer getInteger(String key) {
		return Integer.parseInt(getString(key));
	}

	/**
	 * Booleanの設定値を取得します。
	 * 
	 * @param key
	 *            キー
	 * @return Booleanの設定値
	 */
	public static boolean isTrue(String key) {
		return "true".equals(setting.getString(key));
	}
}
