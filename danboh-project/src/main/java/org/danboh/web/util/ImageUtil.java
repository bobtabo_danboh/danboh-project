package org.danboh.web.util;

import java.io.File;

import magick.ImageInfo;
import magick.MagickImage;

import org.apache.commons.io.FileUtils;

import org.danboh.web.exceptions.AppException;

public class ImageUtil {

	public static byte[] convert(File imageFile, String contentType, int width, boolean isMobile) {
		return convert(imageFile, contentType, 0, width, isMobile);
	}

	public static byte[] convert(File imageFile, String contentType, int height, int width, boolean isMobile) {
		try {
			if (height == 0 && width == 0) {
				// 加工しない
				return FileUtils.readFileToByteArray(imageFile);
			}
			String path = imageFile.getPath();
			MagickImage mi = new MagickImage(new ImageInfo(path));
			// 加工後のサイズを求める
			Double srcWidth = mi.getDimension().getWidth();
			Double srcHeight = mi.getDimension().getHeight();
			Integer[] changingSize = sizeChange(width, height, srcWidth, srcHeight);
			if (changingSize == null) {
				// 加工しない
				return FileUtils.readFileToByteArray(imageFile);
			}
			// サイズ変更
			MagickImage mo = mi.scaleImage(changingSize[0], changingSize[1]);
			return mo.imageToBlob(new ImageInfo());
		} catch (Throwable e) {
			throw new AppException(e);
		}
	}

	private static Integer[] sizeChange(Integer distWidth, Integer distHeight, Double srcWidth, Double srcHeight) {
		Integer[] result = { 0, 0 };
		boolean isBaseWidth;

		if ((distWidth == 0 || distWidth >= srcWidth) && (distHeight == 0 || distHeight >= srcHeight)) {
			// サイズ変更なし
			return null;
		}

		Double srcRate = srcWidth / srcHeight;
		if (distHeight == 0) {
			// 縦の指定なし
			isBaseWidth = true;
		} else if (distWidth == 0) {
			// 横の指定なし
			isBaseWidth = false;
		} else {
			// 縦横の指定あり
			Double distRate = distWidth.doubleValue() / distHeight.doubleValue();
			if (srcRate > distRate) {
				isBaseWidth = true;
			} else {
				isBaseWidth = false;
			}
		}
		if (isBaseWidth) {
			// 横基準で計算
			result[0] = distWidth;
			result[1] = (int) Math.round(distWidth / srcRate);
		} else {
			// 縦基準で計算
			result[0] = (int) Math.round(distHeight * srcRate);
			result[1] = distHeight;
		}
		return result;
	}
}
