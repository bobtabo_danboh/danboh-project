package org.danboh.web.util;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.TimeZone;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.commons.lang.time.DateUtils;

/* $Id: DateUtil.java,v 1.4 2010/08/02 08:37:43 nagashiba Exp $ */

/**
 * 日付関連のユーティリティクラスです。
 * 
 * @author <a href="mailto:nagashiba@adv-co.com">Satoshi Nagashiba</a>
 * @version $Revision: 1.4 $ $Date: 2010/08/02 08:37:43 $
 */
public class DateUtil {

	/**
	 * 日付文字列を日付に変換します。
	 * 
	 * @param date
	 *            日付文字列
	 * @param format
	 *            書式
	 * @return 日付
	 */
	public static Date parse(String date, String format) {
		try {
			return DateUtils.parseDate(date, new String[] { format });
		} catch (ParseException e) {
			return null;
		}
	}

	/**
	 * 年月日数値を日付に変換します。
	 * 
	 * @param yyyy
	 *            年
	 * @param mm
	 *            月
	 * @param dd
	 *            日
	 * @return 日付
	 */
	public static Date parseDate(int yyyy, int mm, int dd) {
		Date date = getToday();
		date = DateUtils.setYears(date, yyyy);
		date = DateUtils.setMonths(date, mm);
		date = DateUtils.setDays(date, dd);
		return date;
	}

	/**
	 * 時刻数値を日付に変換します。
	 * 
	 * @param hh
	 *            時間
	 * @param mm
	 *            分
	 * @param ss
	 *            秒
	 * @return 日付
	 */
	public static Date parseTime(int hh, int mm, int ss) {
		Date date = getToday();
		date = DateUtils.setHours(date, hh);
		date = DateUtils.setMinutes(date, mm);
		date = DateUtils.setSeconds(date, ss);
		return date;
	}

	/**
	 * 年月日時刻数値を日付に変換します。
	 * 
	 * @param yyyy
	 *            年
	 * @param mi
	 *            月
	 * @param dd
	 *            日
	 * @param hh
	 *            時間
	 * @param mm
	 *            分
	 * @param ss
	 *            秒
	 * @return 日付
	 */
	public static Date parseDateTime(int yyyy, int mi, int dd, int hh, int mm, int ss) {
		Date date = getToday();
		date = DateUtils.setYears(date, yyyy);
		date = DateUtils.setMonths(date, mi);
		date = DateUtils.setDays(date, dd);
		date = DateUtils.setHours(date, hh);
		date = DateUtils.setMinutes(date, mm);
		date = DateUtils.setSeconds(date, ss);
		return date;
	}

	/**
	 * 日付をカレンダーに変換します。
	 * 
	 * @param date
	 *            日付
	 * @return カレンダー
	 */
	public static Calendar toCalendar(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		return calendar;
	}

	/**
	 * 現在日付を取得します。
	 * 
	 * @return 現在日付
	 */
	public static Date getToday() {
		return DateUtils.truncate(new Date(), Calendar.DATE);
	}

	/**
	 * システム日時を取得します.
	 * 
	 * @return システム timestamp
	 */
	public static Timestamp getSystemTimestamp() {
		Calendar cal = GregorianCalendar.getInstance();
		return new Timestamp(cal.getTimeInMillis());
	}

	/**
	 * 現在日付文字列を取得します。
	 * 
	 * @return 現在日付文字列
	 */
	public static String getTodayString() {
		return dateToString(getToday(), "yyyy/MM/dd");
	}

	/**
	 * 日付を文字列に変換します。
	 * 
	 * @param date
	 *            日付
	 * @param format
	 *            書式
	 * @return 文字列
	 */
	public static String dateToString(Date date, String format) {
		if (date == null) {
			return null;
		}
		SimpleDateFormat formater = new SimpleDateFormat(format);
		return formater.format(date);
	}

	/**
	 * 日付を文字列に変換します。
	 * 
	 * @param date
	 *            日付
	 * @param format
	 *            書式
	 * @return 文字列
	 */
	public static String dateToString(long date, String format) {
		if (date < 0) {
			return "-";
		}
		return dateToString(new Date(date), format);
	}

	/**
	 * 日付を文字列に変換します。(yyyy/mm/dd固定)
	 * 
	 * @param date
	 *            日付
	 * @return 文字列
	 */
	public static String dateToString(long date) {
		return dateToString(date, "yyyy/MM/dd");
	}

	/**
	 * 年月日時の文字列を日付に変換します。
	 * 
	 * @param yyyy
	 *            年
	 * @param mm
	 *            月
	 * @param dd
	 *            日
	 * @param hh
	 *            時
	 * @return 日付
	 */
	public static Date stringToDate(String yyyy, String mm, String dd, String hh) {
		Date date = DateUtils.truncate(new Date(), Calendar.HOUR);
		date = DateUtils.setYears(date, Integer.parseInt(yyyy));
		date = DateUtils.setMonths(date, Integer.parseInt(mm) - 1);
		date = DateUtils.setDays(date, Integer.parseInt(dd));
		date = DateUtils.setHours(date, Integer.parseInt(hh));
		return date;
	}

	/**
	 * 日付かを確認します。
	 * 
	 * @param date
	 *            日付
	 * @return 不正な日付の場合 false を返します
	 */
	public static boolean isDate(String date) {
		if (StringUtils.isEmpty(date)) {
			throw new NullPointerException("引数は Null もしくは空文字です。");
		}
		date = date.replace('-', '/');
		DateFormat format = DateFormat.getDateInstance();
		// 日付/時刻解析を厳密に行うかどうかを設定する。
		format.setLenient(false);
		try {
			format.parse(date);
			return true;
		} catch (ParseException e) {
			return false;
		}
	}

	/**
	 * 対象日の最終日時（23時59分59秒）を取得します。
	 * 
	 * @param date
	 *            対象日
	 * @return 対象日の最終日時
	 */
	public static Date getDayLastTime(String date) {
		return getDayLastTime(parse(date, "yyyy/MM/dd"));
	}

	/**
	 * 対象日の最終日時（23時59分59秒）を取得します。
	 * 
	 * @param date
	 *            対象日
	 * @return 対象日の最終日時
	 */
	public static Date getDayLastTime(Date date) {
		date = DateUtils.setHours(date, 23);
		date = DateUtils.setMinutes(date, 59);
		date = DateUtils.setSeconds(date, 59);
		return date;
	}

	/**
	 * 対象日の最終日時（23時59分59秒）を取得します。
	 * 
	 * @param date
	 *            対象日
	 * @return 対象日の最終日時
	 */
	public static Timestamp getDayLastTimestamp(Timestamp stamp) {
		Date day = new Date(stamp.getTime());
		day = DateUtils.setHours(day, 23);
		day = DateUtils.setMinutes(day, 59);
		day = DateUtils.setSeconds(day, 59);
		return new Timestamp(day.getTime());
	}

	/**
	 * 本日日時（0時0分0秒）を取得します。
	 * 
	 * @param date
	 *            対象日
	 * @return 対象日の最終日時
	 */
	public static Timestamp getTodayTimestamp() {
		return zeroStamp(getSystemTimestamp());
	}

	/**
	 * 時間をゼロにした日時を取得します。
	 * 
	 * @param stamp
	 *            対象日時
	 * @return 時間をゼロにした日時
	 */
	public static Timestamp zeroStamp(Timestamp stamp) {
		Date day = new Date(stamp.getTime());
		day = DateUtils.setHours(day, 0);
		day = DateUtils.setMinutes(day, 0);
		day = DateUtils.setSeconds(day, 0);
		day = DateUtils.setMilliseconds(day, 0);
		return new Timestamp(day.getTime());
	}

	/**
	 * 対象日の翌月を取得します。
	 * 
	 * @param date
	 *            対象日
	 * @return 対象日の翌月
	 */
	public static Date getNextMonth(String date) {
		return addDate(date, Calendar.MONTH, 1);
	}

	/**
	 * 対象日の先月を取得します。
	 * 
	 * @param date
	 *            対象日
	 * @return 対象日の翌月
	 */
	public static Date getPrevMonth(Date date) {
		return addDate(date, Calendar.MONTH, -1);
	}

	/**
	 * 対象日の翌日を取得します。
	 * 
	 * @param date
	 *            対象日
	 * @return 対象日の翌日
	 */
	public static Date getNextDay(String date) {
		return addDays(date, 1);
	}

	/**
	 * 対象日の前日を取得します。
	 * 
	 * @param date
	 *            対象日
	 * @return 対象日の前日
	 */
	public static Date getPreviousDay(String date) {
		return addDays(date, -1);
	}

	/**
	 * 日付を加算／減算します。
	 * 
	 * @param date
	 *            日付
	 * @param amount
	 *            加算／減算する値
	 * @return 加算／減算された日付
	 */
	public static Date addDays(String date, int amount) {
		return addDate(date, Calendar.DATE, amount);
	}

	/**
	 * 日付を加算／減算します。
	 * 
	 * @param date
	 *            日付文字列
	 * @param date
	 *            日付フィールド値
	 * @param amount
	 *            加算／減算する値
	 * @return 加算／減算された日付
	 */
	public static Date addDate(String date, int calendarField, int amount) {
		return addDate(parse(date, "yyyy/MM/dd"), calendarField, amount);
	}

	/**
	 * 日付を加算／減算します。
	 * 
	 * @param date
	 *            日付
	 * @param date
	 *            日付フィールド値
	 * @param amount
	 *            加算／減算する値
	 * @return 加算／減算された日付
	 */
	public static Date addDate(Date date, int calendarField, int amount) {
		switch (calendarField) {
		case Calendar.YEAR:
			date = DateUtils.addYears(date, amount);
			break;
		case Calendar.MONTH:
			date = DateUtils.addMonths(date, amount);
			break;
		case Calendar.DATE:
			date = DateUtils.addDays(date, amount);
			break;
		}
		return date;
	}

	/**
	 * 日付を加算／減算した日付文字列を取得します。
	 * 
	 * @param date
	 *            日付文字列
	 * @param date
	 *            日付フィールド値
	 * @param amount
	 *            加算／減算する値
	 * @param format
	 *            書式
	 * @return 加算／減算された日付
	 */
	public static String addDateToString(String date, int calendarField, int amount, String format) {
		return dateToString(addDate(date, calendarField, amount), format);
	}

	/**
	 * 対象日時に分を加算したタイムスタンプを取得します。
	 * 
	 * @param date
	 *            対象日時
	 * @param amount
	 *            加算する分
	 * @return タイムスタンプ
	 */
	public static Timestamp addMinuteStamp(Date date, int amount) {
		Date result = DateUtils.addMinutes(date, amount);
		return new Timestamp(result.getTime());
	}

	/**
	 * 対象日までの残り日時文字列を取得します。
	 * 
	 * @param date
	 *            対象日
	 * @return 残り日時文字列
	 */
	public static String getRemain(Date date) {
		long nowTime = Calendar.getInstance().getTimeInMillis();
		long endTime = date.getTime();

		long diff = endTime - nowTime;
		// diff = diff / 60 / 24;

		if (diff < 0) {
			return "終了";
		}

		long dayDiff = Math.abs(getRemainValue(date));

		if (dayDiff > 0) {
			return dayDiff + "日";
		}

		SimpleDateFormat formatter = new SimpleDateFormat("HH:mm:ss");
		formatter.setTimeZone(TimeZone.getTimeZone("GMT"));
		String[] hms = StringUtils.split(formatter.format(new Date(diff)), ":");
		String[] unit = { "時間", "分", "秒" };

		String result = null;

		for (int i = 0; i < hms.length; i++) {
			if (!"00".equals(hms[i])) {
				result = String.valueOf(Integer.parseInt(hms[i])) + unit[i];
				break;
			}
		}

		return result;
	}

	/**
	 * 対象日と本日の差を返します。
	 * 
	 * @param date
	 *            対象日
	 * @return 対象日と本日の差（前日は0より大きい、後日はマイナス）
	 */
	public static int getRemainValue(Date date) {
		long now = getToday().getTime();
		long target = date.getTime();
		long one_date_time = 1000 * 60 * 60 * 24;
		long diffDays = (now - target) / one_date_time;
		return (int) diffDays;
	}

	/**
	 * 対象時刻と現在時刻の差が、指定分前であるか確認します。
	 * 
	 * @param date
	 *            対象時刻
	 * @return 指定分前であれば true を返します
	 */
	public static boolean isRemainMinute(Date date, int minute) {
		long now = Calendar.getInstance().getTimeInMillis();
		long target = date.getTime();
		long diff = target - now;
		int remain = Long.valueOf(diff / 1000).intValue();
		int sec = minute * 60;
		return ((remain > 0) && (remain < sec));
	}

	/**
	 * 対象日が過去日であるか確認します。
	 * 
	 * @param date
	 *            対象日
	 * @return 過去日の場合 true を返します。
	 */
	public static boolean isPastDay(String date) {
		return isPastDay(parse(date, "yyyy/MM/dd"));
	}

	/**
	 * 対象日が過去日であるか確認します。
	 * 
	 * @param date
	 *            対象日
	 * @return 過去日の場合 true を返します。
	 */
	public static boolean isPastDay(Date date) {
		long past = DateUtils.truncate(date, Calendar.DATE).getTime();
		long current = getToday().getTime();
		return past < current;
	}

	/**
	 * 過去日時であるか確認します。
	 * 
	 * @param yyyy
	 *            年
	 * @param mm
	 *            月
	 * @param dd
	 *            日
	 * @param hh
	 *            時
	 * @return 過去日時の場合 true を返します。
	 */
	public static boolean isPastDateTime(String yyyy, String mm, String dd, String hh) {
		return isPastDateTime(stringToDate(yyyy, mm, dd, hh));
	}

	/**
	 * 過去日時であるか確認します。
	 * 
	 * @return 過去日時の場合 true を返します。
	 */
	public static boolean isPastDateTime(Date date) {
		long past = DateUtils.truncate(date, Calendar.HOUR).getTime();
		long current = DateUtils.truncate(getSystemTimestamp(), Calendar.HOUR).getTime();
		return past < current;
	}

	/**
	 * 未来日付であるか確認します。
	 * 
	 * @param date
	 *            日付文字列
	 * @return 未来日付の場合 true を返します
	 */
	public static boolean isFutureDay(String date) {
		return isFutureDay(parse(date, "yyyy/MM/dd"));
	}

	/**
	 * 未来日付であるか確認します。
	 * 
	 * @param date
	 *            日付
	 * @return 未来日付の場合 true を返します
	 */
	public static boolean isFutureDay(Date date) {
		long future = DateUtils.truncate(date, Calendar.DATE).getTime();
		long current = getToday().getTime();
		return future > current;
	}

	/**
	 * 現在日時であるか確認します。
	 * 
	 * @param yyyy
	 *            年
	 * @param mm
	 *            月
	 * @param dd
	 *            日
	 * @param hh
	 *            時
	 * @return 現在日時の場合 true を返します。
	 */
	public static boolean isCurrentDateTime(String yyyy, String mm, String dd, String hh) {
		return isCurrentDateTime(stringToDate(yyyy, mm, dd, hh));
	}

	/**
	 * 現在日時であるか確認します。
	 * 
	 * @return 現在日時の場合 true を返します。
	 */
	public static boolean isCurrentDateTime(Date date) {
		long now = DateUtils.truncate(date, Calendar.HOUR).getTime();
		long current = DateUtils.truncate(getSystemTimestamp(), Calendar.HOUR).getTime();
		return now == current;
	}

	/**
	 * 日付を比較します。
	 * 
	 * @param from
	 *            開始日
	 * @param to
	 *            終了日
	 * @return from > to の場合 false を返します
	 */
	public static boolean isComparisonDate(String from, String to) {
		return isComparisonDate(parse(from, "yyyy/MM/dd"), parse(to, "yyyy/MM/dd"));
	}

	/**
	 * 日付を比較します。
	 * 
	 * @param from
	 *            開始日
	 * @param to
	 *            終了日
	 * @return from > to の場合 false を返します
	 */
	public static boolean isComparisonDate(Date from, Date to) {
		long fromTime = from.getTime();
		long toTime = to.getTime();
		return fromTime <= toTime;
	}

	/**
	 * 時間リストを取得します。
	 * 
	 * @return 時間リスト
	 */
	public static List<String> getTimeList() {
		List<String> result = new ArrayList<String>();
		DecimalFormat formatter = new DecimalFormat("00");
		for (int i = 0; i < 24; i++) {
			result.add(formatter.format(i));
		}
		return result;
	}

	/**
	 * 日付から年文字列を取得します。
	 * 
	 * @param date
	 *            日付
	 * @return 年文字列
	 */
	public static String getYear(Date date) {
		return getDate(date, Calendar.YEAR);
	}

	/**
	 * 日付から月文字列を取得します。
	 * 
	 * @param date
	 *            日付
	 * @return 月文字列
	 */
	public static String getMonth(Date date) {
		return getDate(date, Calendar.MONTH);
	}

	/**
	 * 日付から日文字列を取得します。
	 * 
	 * @param date
	 *            日付
	 * @return 日文字列
	 */
	public static String getDay(Date date) {
		return getDate(date, Calendar.DATE);
	}

	/**
	 * 日付から時文字列を取得します。
	 * 
	 * @param date
	 *            日付
	 * @return 時文字列
	 */
	public static String getHour(Date date) {
		return getDate(date, Calendar.HOUR_OF_DAY);
	}

	/**
	 * 日付から分文字列を取得します。
	 * 
	 * @param date
	 *            日付
	 * @return 分文字列
	 */
	public static String getMinute(Date date) {
		return getDate(date, Calendar.MINUTE);
	}

	/**
	 * 日付から秒文字列を取得します。
	 * 
	 * @param date
	 *            日付
	 * @return 秒文字列
	 */
	public static String getSecond(Date date) {
		return getDate(date, Calendar.SECOND);
	}

	/**
	 * 日付から年数値を取得します。
	 * 
	 * @param date
	 *            日付
	 * @return 年数値
	 */
	public static int getYearValue(Date date) {
		return NumberUtils.toInt(getYear(date));
	}

	/**
	 * 日付から月数値を取得します。
	 * 
	 * @param date
	 *            日付
	 * @return 月数値
	 */
	public static int getMonthValue(Date date) {
		return NumberUtils.toInt(getMonth(date));
	}

	/**
	 * 日付から日数値を取得します。
	 * 
	 * @param date
	 *            日付
	 * @return 日数値
	 */
	public static int getDayValue(Date date) {
		return NumberUtils.toInt(getDay(date));
	}

	/**
	 * 日付から時数値を取得します。
	 * 
	 * @param date
	 *            日付
	 * @return 時数値
	 */
	public static int getHourValue(Date date) {
		return NumberUtils.toInt(getHour(date));
	}

	/**
	 * 日付から分数値を取得します。
	 * 
	 * @param date
	 *            日付
	 * @return 分数値
	 */
	public static int getMinuteValue(Date date) {
		return NumberUtils.toInt(getMinute(date));
	}

	/**
	 * 日付から秒数値を取得します。
	 * 
	 * @param date
	 *            日付
	 * @return 秒数値
	 */
	public static int getSecondValue(Date date) {
		return NumberUtils.toInt(getSecond(date));
	}

	/**
	 * 日付から対象フィールドの文字列を取得します。
	 * 
	 * @param date
	 *            日付
	 * @param calendarField
	 *            カレンダーフィールド
	 * @return 指定フィールド文字列
	 */
	public static String getDate(Date date, int calendarField) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		DecimalFormat formatter = null;
		if (Calendar.YEAR == calendarField) {
			formatter = new DecimalFormat("0000");
		} else {
			formatter = new DecimalFormat("00");
		}
		if (Calendar.MONTH == calendarField) {
			return formatter.format(cal.get(calendarField) + 1);
		}
		return formatter.format(cal.get(calendarField));
	}

	/**
	 * 当月の月初を取得します。
	 * 
	 * @return 当月の月初
	 */
	public static Date getThisMonthFirstDay() {
		Date today = getToday();
		return DateUtils.setDays(today, 1);
	}

	/**
	 * 翌月の月初を取得します。
	 * 
	 * @return 翌月の月初
	 */
	public static Date getNextMonthFirstDay() {
		Date today = getToday();
		Date nextMonth = DateUtils.addMonths(today, 1);
		return DateUtils.setDays(nextMonth, 1);
	}

	/**
	 * 対象日付が範囲内か確認します。
	 * 
	 * @param from
	 *            範囲の下限
	 * @param to
	 *            範囲の上限
	 * @param target
	 *            対象日付
	 * @return 範囲内の場合 true を返します
	 */
	public static boolean isDateRange(Date from, Date to, Date target) {
		long targetValue = target.getTime();
		long fromValue = from.getTime();
		long toValue = to.getTime();
		return ((targetValue >= fromValue) && (targetValue <= toValue));
	}

	/**
	 * 対象日時が本日か確認します。
	 * 
	 * @param stamp
	 *            対象日時
	 * @return 本日の場合 true を返します
	 */
	public static boolean isToday(Timestamp stamp) {
		if (stamp == null) {
			return false;
		}
		long target = zeroStamp(stamp).getTime();
		long today = getTodayTimestamp().getTime();
		return (target == today);
	}
}
