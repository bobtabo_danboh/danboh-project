package org.danboh.web.util;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.apache.struts2.ServletActionContext;
import org.seasar.framework.container.ComponentNotFoundRuntimeException;
import org.seasar.framework.container.S2Container;
import org.seasar.framework.container.factory.SingletonS2ContainerFactory;

/* $Id: ServletUtil.java,v 1.2 2010/06/25 08:18:52 nagashiba Exp $ */

/**
 * サーブレット関連のユーティリティクラスです。
 * 
 * @author <a href="mailto:kosugi@prov-co.com">kosugi</a>
 * @version $Revision: 1.2 $ $Date: 2010/06/25 08:18:52 $
 */
public class ServletUtil {

	/**
	 * コンテキストの絶対パスを取得します。
	 * 
	 * @param path
	 *            相対パス
	 * @return コンテキストの絶対パス
	 */
	public static String getRealPath(String path) {
		S2Container container = SingletonS2ContainerFactory.getContainer();
		ServletContext context = null;
		try {
			context = (ServletContext) container.getComponent(ServletContext.class);
		} catch (ComponentNotFoundRuntimeException e) {
			// サーブレット環境でない場合はパスをそのまま返す
		}

		if (context == null) {
			return path;
		} else {
			return context.getRealPath(path);
		}
	}

	/**
	 * セッションを再構築します。
	 * 
	 * @param httpServletRequest
	 *            HTTPサーブレットリクエスト
	 */
	@SuppressWarnings("unchecked")
	public static void rebuildSession(HttpServletRequest httpServletRequest) {
		Map<String, Object> sessionAttributes = new HashMap<String, Object>();
		HttpSession httpSession = httpServletRequest.getSession();
		for (Enumeration<String> enumeration = httpSession.getAttributeNames(); enumeration.hasMoreElements();) {
			String attributeName = (String) enumeration.nextElement();
			sessionAttributes.put(attributeName, httpSession.getAttribute(attributeName));
		}
		httpSession.invalidate();
		httpSession = httpServletRequest.getSession();
		for (Entry<String, Object> entry : sessionAttributes.entrySet()) {
			httpSession.setAttribute(entry.getKey(), entry.getValue());
		}
	}

	/**
	 * リダイレクトURLを作成します。
	 * 
	 * @param httpServletRequest
	 *            HTTPサーブレットリクエスト
	 * @param httpServletResponse
	 *            HTTPサーブレットレスポンス
	 * @param fromScheme
	 *            置換されるURLスキーマ
	 * @param toScheme
	 *            置換するURLスキーマ
	 * @return 作成されたURL
	 */
	public static String createRedirectUrl(HttpServletRequest httpServletRequest,
			HttpServletResponse httpServletResponse, String fromScheme, String toScheme) {
		String redirectUrl = httpServletResponse.encodeURL(httpServletRequest.getRequestURL().toString());
		if (StringUtils.isNotBlank(fromScheme) && StringUtils.isNotBlank(toScheme)) {
			redirectUrl = redirectUrl.replace(fromScheme, toScheme);
		}
		String queryString = httpServletRequest.getQueryString();
		if (!StringUtils.isBlank(queryString)) {
			redirectUrl = String.format("%s?%s", redirectUrl, queryString);
		}
		return redirectUrl;
	}

	/**
	 * コンテキストパスを取得します。
	 * 
	 * @return コンテキストパス
	 */
	public static String getContextPath() {
		return ServletActionContext.getRequest().getContextPath();
	}
}
