package org.danboh.web.task.main;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.seasar.chronos.core.Scheduler;
import org.seasar.framework.container.SingletonS2Container;
import org.seasar.framework.container.factory.SingletonS2ContainerFactory;

/**
 * バッチ起動クラスです。
 * 
 * @author <a href="mailto:kosugi@prov-co.com">kosugi</a>
 * @version $Revision: 1.1 $ $Date: 2010/06/11 04:03:41 $
 */
public final class TaskStarter {
	@SuppressWarnings("unused")
	private static Log log = LogFactory.getLog(TaskStarter.class);

	private TaskStarter() {
	}

	/**
	 * タスクを起動するエントリーポイント。
	 */
	public static void main(String[] args) {
		SingletonS2ContainerFactory.setConfigPath("app-task.dicon");
		SingletonS2ContainerFactory.init();
		SingletonS2Container.getComponent(Scheduler.class).process();
		SingletonS2ContainerFactory.destroy();
	}
}
