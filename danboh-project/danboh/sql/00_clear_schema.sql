SET FOREIGN_KEY_CHECKS=0;

--
-- 管理者
--
DROP TABLE IF EXISTS `MANAGER` ;
--
-- 会員
--
DROP TABLE IF EXISTS `MEMBER` ;
--
-- 会員グループ
--
DROP TABLE IF EXISTS `MEMBER_GROUP` ;
--
-- お知らせ
--
DROP TABLE IF EXISTS `ANNOUNCE` ;
--
-- RCバージョン
--
DROP TABLE IF EXISTS `RC_VERSION` ;
--
-- アップロード
--
DROP TABLE IF EXISTS `UPLOAD` ;
--
-- ライセンス履歴
--
DROP TABLE IF EXISTS `LICENSE_HISTORY` ;
--
-- クライアント
--
DROP TABLE IF EXISTS `CLIENT` ;
--
-- クライアント画像
--
DROP TABLE IF EXISTS `CLIENT_IMAGE` ;
--
-- サイト情報グループ
--
DROP TABLE IF EXISTS `SITE_INFO_GROUP` ;
--
-- サイト情報
--
DROP TABLE IF EXISTS `SITE_INFO` ;
--
-- 項目グループ
--
DROP TABLE IF EXISTS `ITEM_GROUP` ;
--
-- 調査データ
--
DROP TABLE IF EXISTS `INQUIRY_DATA` ;
--
-- 調査項目
--
DROP TABLE IF EXISTS `ITEM` ;
--
-- 調査項目詳細
--
DROP TABLE IF EXISTS `ITEM_DETAIL` ;

SET FOREIGN_KEY_CHECKS=1;
